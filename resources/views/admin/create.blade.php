@extends('main')
@section('content')


<link href="{{ asset('assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />

<link href="{{ asset('sweetalert/dist/sweetalert.css')}}" rel="stylesheet" type="text/css" />


<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content"><br/>


	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="kt-font-brand flaticon2-line-chart"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						Add {{ $title }}
					</h3>
				</div>
			</div>
			<div class="kt-portlet__body">
				<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
					<form class="kt-form add__form" id="add__form" method="post">
						@csrf
						<div class="row">
							<div class="col-sm-4">
								<label>First Name<code>*</code></label>
								<input type="text" class="form-control" name="name" id="name" placeholder="Enter First Name" autocomplete="off">
							</div>
							<div class="col-sm-4">
								<label>Last Name<code>*</code></label>
								<input type="text" class="form-control" name="lname" id="lname" placeholder="Enter Last Name" autocomplete="off">
							</div>
							<div class="col-sm-4">
								<label>Email<code>*</code></label>
								<input type="text" class="form-control" name="email" id="email" placeholder="Enter Email" autocomplete="off">
							</div>
							<div class="col-sm-4">
								<label>Password<code>*</code></label>
								<input type="password" class="form-control" name="password" id="password" placeholder="Enter Password" autocomplete="off">
							</div>
							<div class="col-sm-4">
								<label>Confirm Password<code>*</code></label>
								<input type="password" class="form-control" name="cPassword" id="cPassword" placeholder="Enter Confirm Password" autocomplete="off">
							</div>
							<div class="col-sm-4">
								<label>Mobile No.<code>*</code></label>
								<input type="text" class="form-control" name="contact" id="contact" placeholder="Enter Mobile No." autocomplete="off">
							</div>
						</div>
						<div class="kt-portlet__foot">
							<div class="kt-form__actions">
								<button type="button" class="btn btn-brand" id="save">Submit</button>
								<a href="{{route('user.index')}}" class="btn btn-dark">Cancel</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

</div>

<script>
	$(".add__form").validate({
		rules:
		{
			name:{required:true},
			lname:{required:true},
			email:{
				required: true,
				email: true,
				remote: {
					url: "{{ route('check.adminEmail') }}",
					type: "post",
					dataType: 'json',
					data: {
						'_token': $('input[name="_token"]').val(),
						email: function() {
							return $( "#email" ).val();
						}
					}
				}
			},
			password:{required:true},
			cPassword:{required:true,equalTo:"#password"},
			contact:{required:true},
		},
		messages:
		{
			name:{required:"Please enter first name"},
			lname:{required:"Please enter last name"},
			email:{
				required:"Please enter email id",
				email:"Please enter valid formate",
				remote:"This email is already exist"
			},
			password:{required:"Please enter password"},
			cPassword:{required:"Please enter confirm password",equalTo:"Please enter same as password"},
			contact:{required:"Please enter mobile no."}
		}
	});

	$("#save").on("click", function (e)
	{
		e.preventDefault();
		if ($(".add__form").valid())
		{
			$.ajax({
				type: "POST",
				url: "{{ route('admin.store') }}",
				data: new FormData($('.add__form')[0]),
				processData: false,
				contentType: false,
				success: function (data)
				{
					if (data.status === 'success') 
					{
						toastr.options.timeOut = 3000;
						toastr.options.fadeOut = 3000;
						toastr.options.progressBar = true;
						toastr.options.onHidden = function(){
							window.location = "{{ route('admin.index') }}"
						};

						toastr["success"]("Admin Added Successfully", "Success");
					}
					else if(data.status === 'error') 
					{
						toastr.options.timeOut = 3000;
						toastr.options.fadeOut = 3000;
						toastr.options.progressBar = true;

						toastr["error"]("Opps.. Something Went Wrong.!", "Error");
					}
				}
			});
		}
		else
		{
			e.preventDefault();
		}
	});
</script>
@stop