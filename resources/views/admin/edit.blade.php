@extends('main')
@section('content')


<link href="{{ asset('assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />

<link href="{{ asset('sweetalert/dist/sweetalert.css')}}" rel="stylesheet" type="text/css" />


<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content"><br/>


	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="kt-font-brand flaticon2-line-chart"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						Add {{ $title }}
					</h3>
				</div>
			</div>
			<div class="kt-portlet__body">
				<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
					<form class="kt-form edit__form" id="edit__form" method="POST">
						@csrf
						@method('PUT')
						<input type="hidden" id="user_id" value="{{ $edit->id }}">
						<div class="row">
							<div class="col-sm-4">
								<label>First Name<code>*</code></label>
								<input type="text" class="form-control" name="name" id="name" placeholder="Enter First Name" autocomplete="off" value="{{ $edit->name }}">
							</div>
							<div class="col-sm-4">
								<label>Last Name<code>*</code></label>
								<input type="text" class="form-control" name="lname" id="lname" placeholder="Enter Last Name" autocomplete="off" value="{{ $edit->lname }}">
							</div>
							<div class="col-sm-4">
								<label>Email<code>*</code></label>
								<input type="text" class="form-control" name="email" id="email" placeholder="Enter Email" autocomplete="off" value="{{ $edit->email }}">
							</div>
							<div class="col-sm-4">
								<label>Mobile No.<code>*</code></label>
								<input type="text" class="form-control" name="contact" id="contact" placeholder="Enter Mobile No." autocomplete="off" value="{{ $edit->contact }}">
							</div>
						</div>
						<div class="kt-portlet__foot">
							<div class="kt-form__actions">
								<button type="button" class="btn btn-brand" id="save">Submit</button>
								<a href="{{route('admin.index')}}" class="btn btn-dark">Cancel</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

</div>

<script>
	$(".edit__form").validate({
		rules:
		{
			role_id:{required:true},
			name:{required:true},
			lname:{required:true},
			email:{
				required: true,
				email: true,
				remote: {
					url: "{{ route('check.adminEmail') }}",
					type: "post",
					dataType: 'json',
					data: {
						'_token': $('input[name="_token"]').val(),
						id: function() {
							return $( "#user_id" ).val();
						},
						email: function() {
							return $( "#email" ).val();
						}
					}
				}
			},
			contact:{required:true},
			school_name:{required:true}
		},
		messages:
		{
			role_id:{required:"Please select role"},
			name:{required:"Please enter first name"},
			lname:{required:"Please enter last name"},
			email:{
				required:"Please enter email id",
				email:"Please enter valid formate",
				remote:"This email is already exist"
			},
			contact:{required:"Please enter mobile no."},
			school_name:{required:"Please enter school name"}
		}
	});

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
		}
	});

	$("#save").on("click", function (e)
	{
		e.preventDefault();
		if ($(".edit__form").valid())
		{
			$.ajax({
				type: "POST",
				url: "{{ route('admin.update', array($edit->id)) }}",
				data: new FormData($('.edit__form')[0]),
				processData: false,
				contentType: false,
				success: function (data)
				{
					if (data.status === 'success') 
					{
						toastr.options.timeOut = 3000;
						toastr.options.fadeOut = 3000;
						toastr.options.progressBar = true;
						toastr.options.onHidden = function(){
							window.location = "{{ route('admin.index') }}"
						};

						toastr["success"]("User Updated Successfully", "Success");
					}
					else if(data.status === 'error') 
					{
						toastr.options.timeOut = 3000;
						toastr.options.fadeOut = 3000;
						toastr.options.progressBar = true;

						toastr["error"]("Opps.. Something Went Wrong.!", "Error");
					}
				}
			});
		}
		else
		{
			e.preventDefault();
		}
	});
</script>
@stop