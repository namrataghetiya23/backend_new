@extends('main')

@section('content')



<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content"><br/>



	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

		<div class="kt-portlet kt-portlet--mobile">

			<div class="kt-portlet__head kt-portlet__head--lg">

				<div class="kt-portlet__head-label">

					<span class="kt-portlet__head-icon">

						<i class="kt-font-brand flaticon2-line-chart"></i>

					</span>

					<h3 class="kt-portlet__head-title">

						{{ $title }} List

					</h3>

				</div>

				<div class="kt-portlet__head-toolbar">

					<div class="kt-portlet__head-wrapper">



						<div class="kt-portlet__head-actions">

							

							<a type="button" class="btn btn-brand btn-elevate btn-icon-sm" data-toggle="modal" data-target="#add_category" style="color: #ffffff;">

								<i class="la la-plus"></i>

								Add {{ $title }}

							</a>

							

						</div>

						

					</div>

				</div>

			</div>

			<div class="kt-portlet__body">

				<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

					

					<div class="row">

						<div class="col-sm-12" id="category_list_table">

							<table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="category_list" role="grid" aria-describedby="kt_table_1_info">

								@csrf



								<thead>

									<tr role="row">

										<th>No</th>

										<th>Category</th>

										<th>Sub Category</th>

										<th>Activate Status</th>

										<th>Actions</th>

									</tr>

								</thead>

								<tbody>



									@if(!empty($data))

									<?php $count = 1; ?>

									@foreach($data as $dt)

									<tr>

										<td>{{ $count++ }}</td>

										<td>{{ ucfirst($dt->name) }}</td>

										<td>{{ ucfirst($dt->sub_name) }}</td>

										<td>

											<span class="kt-switch kt-switch--sm">

												<label>

													<input type="checkbox" @if($dt->active == 1) checked @endif onchange="status('{{ $dt->id }}')" name="" id="check{{ $dt->id }}">

													<span></span>

												</label>

											</span>

										</td>

										<td>

											<a data-toggle="modal" data-target="#edit_category" onclick="edit('{{ $dt->id }}')" class="btn btn-sm btn-clean btn-icon btn-icon-md edit-icon" title="Edit" style="background: #968cbd;">

												<i class="la la-edit" style="color: #ffffff"></i>

											</a>

											<input type="hidden" value="{{ $dt->id }}" name="delete_subCategory_id" id="delete_subCategory_id">



											<button class="btn btn-sm btn-clean btn-icon btn-icon-md" id="delete_subCategory" style="background-color: red;" data-toggle="tooltip" title="Delete">

												<i class="la la-trash" style="color: #ffffff"></i>

											</button>

										</td>

									</tr>

									@endforeach

									@endif

								</tbody>

							</table>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>



<!-- Add Modal-->

<div class="modal fade" id="add_category" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">

	<div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<h5 class="modal-title" id="exampleModalLabel">Add {{ $title }}</h5>

				<button type="button" class="close" data-dismiss="modal" aria-label="Close">

					<i aria-hidden="true" class="ki ki-close"></i>

				</button>

			</div>

			<form action="{{ route('subCategory.store') }}" method="post" class="add__form">

				@csrf

				<div class="modal-body">

					<div data-scroll="true" data-height="auto">

						<div class="form-group row">

							<label class="col-xl-3 col-lg-3 col-form-label">Category</label>

							<div class="col-lg-9 col-xl-9">

								<select required="required" class="form-control" id="cat_id" name="cat_id">

									<option value="">--Select Category--</option>

									@if(!empty($category))

									@foreach($category as $cat_dt)

									@if($cat_dt->type == 1)

									@php $type = "Subject" @endphp

									@else

									@php $type = "Course" @endphp

									@endif

									<option value="{{ $cat_dt->id }}">{{ $cat_dt->name }} ( {{ $type }} )</option>

									@endforeach

									@endif

								</select>

							</div>

						</div>

						<div class="form-group row">

							<label class="col-xl-3 col-lg-3 col-form-label">Sub Category Name</label>

							<div class="col-lg-9 col-xl-9">

								<input type="text" class="form-control" required="required" id="sub_name" name="sub_name" placeholder="Add Sub Category Name" autocomplete="off">

							</div>

						</div>

						<div class="modal-footer">

							<button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>

							<button type="button" id="save" class="btn btn-brand btn-elevate btn-icon-sm font-weight-bold">Add</button>

						</div>

					</div>

				</div>

			</form>

		</div>

	</div>

</div>



<!--Edit Modal-->

<div class="modal fade" id="edit_category" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">

	<div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<h5 class="modal-title" id="exampleModalLabel">Edit {{ $title }}</h5>

				<button type="button" class="close" data-dismiss="modal" aria-label="Close">

					<i aria-hidden="true" class="ki ki-close"></i>

				</button>

			</div>

			<div class="modal-body">

				<form method="post" class="edit__form">

					@csrf

					<input type="hidden" name="subcategory_hidden_id" value="" id="hidden_id">

					<div data-scroll="true" data-height="auto">

						<div class="form-group row">

							<label class="col-xl-3 col-lg-3 col-form-label">Category</label>

							<div class="col-lg-9 col-xl-9">

								<select required="required" class="form-control" id="edit_cat_id" name="cat_id">

									<option value="">--Select Category--</option>

									@if(!empty($category))

									@foreach($category as $cat_dt)

									@if($cat_dt->type == 1)

									@php $type = "Subject" @endphp

									@else

									@php $type = "Course" @endphp

									@endif

									<option value="{{ $cat_dt->id }}">{{ $cat_dt->name }} ( {{ $type }} )</option>

									@endforeach

									@endif

								</select>

							</div>

						</div>

						<div class="form-group row">

							<label class="col-xl-3 col-lg-3 col-form-label">Sub Category Name</label>

							<div class="col-lg-9 col-xl-9">

								<input type="text" class="form-control" required="required" id="edit_sub_name" name="sub_name" placeholder="Add Sub Category Name" autocomplete="off">

							</div>

						</div>

						<div class="modal-footer">

							<button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>

							<button type="button" id="edit" class="btn btn-brand btn-elevate btn-icon-sm font-weight-bold">Update</button>

						</div>

					</div>

				</div>

			</form>

		</div>

	</div>

</div>

<script>

	$(document).ready(function() {

		$('#category_list').DataTable();

	});

</script>

<script>


	function status(id){

		$.ajax({

			type:'POST',

			url:"{{route('subCategory.status')}}",

			data:{

				'_token' : $('input[name="_token"]').val(),

				'id':id

			},

			success:function(data){

				if (data.status === 'status_changed') 

				{

					toastr.options.timeOut = 1500;

					toastr.options.fadeOut = 1500;

					toastr.options.progressBar = true;

					toastr.options.onHidden = function(){

						// location.reload();

					};



					toastr["success"]("Activation Status Changed", "Success");

				}

				if (data.status === 'active_category') 

				{

					toastr.options.timeOut = 3500;

					toastr.options.fadeOut = 3500;

					toastr.options.progressBar = true;

					toastr.options.onHidden = function(){

						location.reload();

					};

					$("#check"+id).prop('checked',true);

					toastr["warning"]("Please Active It's Category Before Active This Sub Category", "Success");

				}

			}

		});

	}   



	function edit(id){



		$.ajax({

			type: "post",

			url: "{{ route('subCategory.edits') }}",

			data: {

				'_token': $('input[name="_token"]').val(),

				'id': id

			},

			cache: false,

			success: function (data)

			{

				$('#hidden_id').val(data.subCategory_data['id']);

				$('#edit_sub_name').val(data.subCategory_data['sub_name']);

				$('#edit_cat_id').val(data.subCategory_data['cat_id']);

			}

		});

	}



	$(document).ready(function() {



		$(document).on('click', '#delete_subCategory', function ()

		{

			var obj = $(this);

			var id = $(this).closest('td').find("#delete_subCategory_id").val();



			swal({

				title: "Are you sure?",

				text: "You will not be able recover this record",

				type: "warning",

				showCancelButton: true,

				confirmButtonColor: "#DD6B55",

				confirmButtonText: "Yes delete it!",

				closeOnConfirm: false

			},

			function () {

				$.ajax({

					type: "post",

					url: '{{ route('delete.subCategory') }}',

					data: {

						'_token': $('input[name="_token"]').val(),

						'id': id

					},

					cache: false,

					success: function (data)

					{

						obj.closest('tr').remove();

					}

				});

				swal("Deleted", "{{ $title }} has been deleted.", "success");

			});

		});

	});

	

	$(".add__form").validate({

		rules:

		{

			cat_id:{required:true},

			sub_name:{

				required: true,

				remote: {

					url: "{{ route('check.subCatName') }}",

					type: "post",

					dataType: 'json',

					data: {

						'_token': $('input[name="_token"]').val(),

						cat_id: function() {

							return $( "#cat_id").val();

						},

						sub_name: function() {

							return $( "#sub_name").val();

						}

					}

				}

			},

		},

		messages:

		{

			cat_id:{required:"Please select category"},

			sub_name:{

				required:"Please enter sub category name",

				remote:"This sub category is already exist"

			},

		}

	});



	$(".edit__form").validate({

		rules:

		{

			cat_id:{required:true},

			sub_name:{

				required: true,

				remote: {

					url: "{{ route('check.subCatName') }}",

					type: "post",

					dataType: 'json',

					data: {

						'_token': $('input[name="_token"]').val(),

						id: function() {

							return $( "#hidden_id" ).val();

						},

						cat_id: function() {

							return $( "#edit_cat_id").val();

						},

						sub_name: function() {

							return $( "#edit_sub_name").val();

						}

					}

				}

			},

		},

		messages:

		{

			cat_id:{required:"Please select category"},

			sub_name:{

				required:"Please enter sub category name",

				remote:"This sub category is already exist"

			},

		}

	});



	$("#edit").on("click", function (e)

	{

		e.preventDefault();

		if ($(".edit__form").valid())

		{

			$.ajax({

				type: "POST",

				url: "{{ route('update.subCategory') }}",

				data: new FormData($('.edit__form')[0]),

				processData: false,

				contentType: false,

				success: function (data)

				{

					if (data.status === 'success') 

					{

						toastr.options.timeOut = 3000;

						toastr.options.fadeOut = 3000;

						toastr.options.progressBar = true;

						toastr.options.onHidden = function(){

							window.location = "{{ route('subCategory.index') }}"

						};



						toastr["success"]("Sub Category Updated Successfully", "Success");

					}

					else if(data.status === 'error') 

					{

						toastr.options.timeOut = 3000;

						toastr.options.fadeOut = 3000;

						toastr.options.progressBar = true;

						toastr.options.onHidden = function(){

							window.location = "{{ route('subCategory.index') }}"

						};



						toastr["error"]("Opps.. Something Went Wrong.!", "Error");

					}

				}

			});

		}

		else

		{

			e.preventDefault();

		}

	});

	$("#save").on("click", function (e)

	{

		e.preventDefault();

		if ($(".add__form").valid())

		{

			$.ajax({

				type: "POST",

				url: "{{ route('subCategory.store') }}",

				data: new FormData($('.add__form')[0]),

				processData: false,

				contentType: false,

				success: function (data)

				{

					if (data.status === 'success') 

					{

						toastr.options.timeOut = 3000;

						toastr.options.fadeOut = 3000;

						toastr.options.progressBar = true;

						toastr.options.onHidden = function(){

							window.location = "{{ route('subCategory.index') }}"

						};



						toastr["success"]("Sub Category Added Successfully", "Success");

					}

					else if(data.status === 'error') 

					{

						toastr.options.timeOut = 3000;

						toastr.options.fadeOut = 3000;

						toastr.options.progressBar = true;



						toastr["error"]("Opps.. Something Went Wrong.!", "Error");

					}

				}

			});

		}

		else

		{

			e.preventDefault();

		}

	});


</script>



@stop