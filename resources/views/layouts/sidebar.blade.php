<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">

	<div class="kt-header-mobile__logo">

		<a href="index.html">

			<!-- <img alt="Logo" src="{{ asset('/assets/media/logos/logo-6-sm.png') }}" /> -->

			<h1 style="font-family: Showcard Gothic;color: #ffffff;">o</h1>

		</a>

	</div>

	<div class="kt-header-mobile__toolbar">

		<div class="kt-header-mobile__toolbar-toggler kt-header-mobile__toolbar-toggler--left" id="kt_aside_mobile_toggler">

			<span></span>

		</div>

		<div class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler">

			<span></span>

		</div>

		<div class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler">

			<i class="flaticon-more"></i>

		</div>

	</div>

</div>



<div class="kt-grid kt-grid--hor kt-grid--root">

	<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

		<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>

		<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">

			<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">

				<div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">

					<ul class="kt-menu__nav ">





						<li class="kt-menu__item" aria-haspopup="true">

							<a href="{{ route('home') }}" class="kt-menu__link ">

								<i class="kt-menu__link-icon flaticon2-protection"></i>

								<span class="kt-menu__link-text">Dashboard</span>

							</a>

						</li>



						



						<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon-squares"></i><span class="kt-menu__link-text">Master</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>

							<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>

								<!-- <ul class="kt-menu__subnav">

									<li class="kt-menu__item " aria-haspopup="true"><a href="{{route('roles.index')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">User Role</span></a></li>

								</ul> -->

								<ul class="kt-menu__subnav">

									<li class="kt-menu__item " aria-haspopup="true"><a href="{{route('user.index')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">User</span></a></li>

								</ul>

								<ul class="kt-menu__subnav">

									<li class="kt-menu__item " aria-haspopup="true"><a href="{{route('admin.index')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Admin</span></a></li>

								</ul>

							</div>

						</li>

						<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">

							<a href="{{route('review.index')}}" class="kt-menu__link">

								<i class="kt-menu__link-icon flaticon2-hourglass-1"></i>

								<span class="kt-menu__link-text">Review</span>

							</a>

						</li>

						<!-- <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">

							<a href="{{route('session.index')}}" class="kt-menu__link">

								<i class="kt-menu__link-icon flaticon2-rocket"></i>

								<span class="kt-menu__link-text">Session</span>

							</a>

						</li> -->

						<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">

							<a href="{{route('payment.index')}}" class="kt-menu__link">

								<i class="kt-menu__link-icon flaticon2-download"></i>

								<span class="kt-menu__link-text">Payment</span>

							</a>

						</li>

						<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">

							<a href="{{route('redemption.index')}}" class="kt-menu__link">

								<i class="kt-menu__link-icon flaticon2-chronometer"></i>

								<span class="kt-menu__link-text">Redemption Requests</span>

							</a>

						</li>

						<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">

							<a href="{{route('category.index')}}" class="kt-menu__link">

								<i class="kt-menu__link-icon flaticon2-paper"></i>

								<span class="kt-menu__link-text">Category</span>

							</a>

						</li>



						<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">

							<a href="{{route('subCategory.index')}}" class="kt-menu__link">

								<i class="kt-menu__link-icon flaticon2-list-3"></i>

								<span class="kt-menu__link-text">Sub Category</span>

							</a>

						</li>


						<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">

							<a href="{{route('course.index')}}" class="kt-menu__link">

								<i class="kt-menu__link-icon flaticon2-box"></i>

								<span class="kt-menu__link-text">Course</span>

							</a>

						</li>



					</ul>

				</div>

			</div>

		</div>

		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

			<div id="kt_header" class="kt-header kt-grid kt-grid--ver  kt-header--fixed ">

				<div class="kt-header__brand kt-grid__item  " id="kt_header_brand">

					<div class="kt-header__brand-logo">

						<a href="index.html">

							<!-- <img alt="Logo" src="{{ asset('assets/media/logos/logo-6.png') }}" /> -->

							<h1 style="font-family: Showcard Gothic;color: #ffffff;">o</h1>

						</a>

					</div>

				</div>

				<h3 class="kt-header__title kt-grid__item">

					Online Learning Plateform

				</h3>

				<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>

				<div class="kt-header-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_header_menu_wrapper">

					<div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">

						<ul class="kt-menu__nav ">

							<li class="kt-menu__item  kt-menu__item--active " aria-haspopup="true">

								<a href="{{ route('home') }}" class="kt-menu__link ">

									<span class="kt-menu__link-text">Dashboard</span>

								</a>

							</li>



							<li class="kt-menu__item  kt-menu__item--active " aria-haspopup="true">

								<a href="{{route('banner.index')}}" class="kt-menu__link ">

									<span class="kt-menu__link-text">Home Banner</span>

								</a>

							</li>



							<li class="kt-menu__item  kt-menu__item--active " aria-haspopup="true">

								<a href="{{ route('staticPage.index') }}" class="kt-menu__link ">

									<span class="kt-menu__link-text">Static Page</span>

								</a>

							</li>

							<!-- <li class="kt-menu__item  kt-menu__item--active " aria-haspopup="true">

								<a href="{{ route('testimonials.index') }}" class="kt-menu__link ">

									<span class="kt-menu__link-text">Testimonials</span>

								</a>

							</li> -->

							<!--<li class="kt-menu__item kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true">

								<a href="javascript:;" class="kt-menu__link kt-menu__toggle">

									<span class="kt-menu__link-text">Manage Content (CMS)</span>

									<i class="kt-menu__hor-arrow la la-angle-down"></i>

									<i class="kt-menu__ver-arrow la la-angle-right"></i>

								</a>

								 <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">

									<ul class="kt-menu__subnav">

										<li class="kt-menu__item  kt-menu__item--submenu" data-ktmenu-submenu-toggle="hover" aria-haspopup="true">

											<a href="javascript:;" class="kt-menu__link kt-menu__toggle">

												<i class="kt-menu__link-icon flaticon2-start-up"></i>

												<span class="kt-menu__link-text">Base</span>

											</a>

										</li>

									</ul>

								</div> 

							</li>-->

							

						</ul>

					</div>

				</div>



				<div class="kt-header__topbar">

					<div class="kt-header__topbar-item kt-header__topbar-item--user">

						<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">

							{{-- <span class="kt-hidden kt-header__topbar-welcome">Hi,</span>

							<span class="kt-hidden kt-header__topbar-username">Nick</span> --}}

							<img class="kt-hidden" alt="Pic" src="{{ asset('assets/media/users/300_21.jpg')}}" />

							<span class="kt-header__topbar-icon kt-hidden-"><i class="flaticon2-user-outline-symbol"></i></span>

						</div>

						<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">



							<div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-color: #445f2194">

								

								<div class="kt-user-card__name">

									

								</div>

							</div>





							<div class="kt-notification">

								<a href="{{route('profile.index')}}" class="kt-notification__item">

									<div class="kt-notification__item-icon">

										<i class="flaticon2-calendar-3 kt-font-success"></i>

									</div>

									<div class="kt-notification__item-details">

										<div class="kt-notification__item-title kt-font-bold">

											My Profile

										</div>

									</div>

								</a>

								<div class="kt-notification__custom kt-space-between">

									<a onclick="event.preventDefault();

									document.getElementById('logout-form').submit();" href="{{ route('logout') }}" class="btn btn-label btn-label-brand btn-sm btn-bold">Sign Out</a>



									<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">

										@csrf

									</form>

								</div>

							</div>



						</div>

					</div>

				</div>

			</div>





