@extends('main')
@section('content')

<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content"><br/>

	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="kt-font-brand flaticon2-line-chart"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						{{ $title }} List
					</h3>
				</div>
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">

						<div class="kt-portlet__head-actions">
							
							<a type="button" class="btn btn-brand btn-elevate btn-icon-sm" data-toggle="modal" data-target="#add_water" style="color: #ffffff;">
								<i class="la la-plus"></i>
								Add {{ $title }}
							</a>
							
						</div>
						
					</div>
				</div>
			</div>
			<div class="kt-portlet__body">
				<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
					<div class="row">
						<div class="col-sm-12">
							<table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="user_list" role="grid" aria-describedby="kt_table_1_info">
								@csrf

								<thead>
									<tr role="row">
										<th>No</th>
										<th>User Role</th>
										<th>Activate Status</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>

									@if(!empty($data))
									<?php $count = 1; ?>
									@foreach($data as $dt)
									<tr>
										<td>{{ $count++ }}</td>
										<td>{{ ucfirst($dt->title) }}</td>
										<td>
											<span class="kt-switch kt-switch--sm">
												<label>
													<input type="checkbox" @if($dt->active == 1) checked @endif onchange="status('{{ $dt->id }}')" name="">
													<span></span>
												</label>
											</span>
										</td>
										<td>

											<a data-toggle="modal" data-target="#edit_water" onclick="edit('{{ $dt->id }}')" class="btn btn-sm btn-clean btn-icon btn-icon-md edit-icon" title="Edit" style="background: #968cbd;">
												<i class="la la-edit" style="color: #ffffff"></i>
											</a>

										</td>
									</tr>
									@endforeach
									@endif
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Add Modal-->
<div class="modal fade" id="add_water" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
	<div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add {{ $title }}</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i aria-hidden="true" class="ki ki-close"></i>
				</button>
			</div>
			<form action="{{ route('roles.store') }}" method="post">
				@csrf
				<div class="modal-body">
					<div data-scroll="true" data-height="auto">
						<div class="form-group row">
							<label class="col-xl-3 col-lg-3 col-form-label">User Role</label>
							<div class="col-lg-9 col-xl-9">
								<input type="text" class="form-control" required="required" name="title" placeholder="Add User Role" autocomplete="off">
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-brand btn-elevate btn-icon-sm font-weight-bold">Add</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<!--Edit Modal-->
<div class="modal fade" id="edit_water" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
	<div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Edit {{ $title }}</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i aria-hidden="true" class="ki ki-close"></i>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{ route('update.roles') }}" method="post">
					@csrf
					<div data-scroll="true" data-height="auto">
						<div class="form-group row">
							<label class="col-xl-3 col-lg-3 col-form-label">User Role</label>
							<div class="col-lg-9 col-xl-9">
								<input type="hidden" name="role_hidden_id" value="" id="hidden_id">
								<input type="text" class="form-control" id="edit_role" required="required" name="title" placeholder="Add User Role" autocomplete="off">
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-brand btn-elevate btn-icon-sm font-weight-bold">Update</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script>
	$(document).ready(function() {
		$('#user_list').DataTable();
	});
</script>
<script>
	function status(id){
		$.ajax({
			type:'POST',
			url:"{{route('role.status')}}",
			data:{
				'_token' : $('input[name="_token"]').val(),
				'id':id
			},
			success:function(data){
				if (data.status === 'status_changed') 
				{
					toastr.options.timeOut = 1500;
					toastr.options.fadeOut = 1500;
					toastr.options.progressBar = true;
					toastr.options.onHidden = function(){
						// location.reload();
					};

					toastr["success"]("Activation Status Changed", "Success");
				}
			}
		});
	}	

	function edit(id){

		$.ajax({
			type: "post",
			url: "{{ route('roles.edits') }}",
			data: {
				'_token': $('input[name="_token"]').val(),
				'id': id
			},
			cache: false,
			success: function (data)
			{
				$('#hidden_id').val(data.role_data['id']);
				$('#edit_role').val(data.role_data['title']);
			}
		});
	}

	
	
</script>

@stop