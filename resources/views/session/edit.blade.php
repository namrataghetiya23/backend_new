@extends('main')

@section('content')



<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content"><br/>





	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

		<div class="kt-portlet kt-portlet--mobile">

			<div class="kt-portlet__head kt-portlet__head--lg">

				<div class="kt-portlet__head-label">

					<span class="kt-portlet__head-icon">

						<i class="kt-font-brand flaticon2-line-chart"></i>

					</span>

					<h3 class="kt-portlet__head-title">

						Add {{ $title }}

					</h3>

				</div>

			</div>

			<div class="kt-portlet__body">

				<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

					<form class="kt-form edit__form" id="edit__form" method="post" enctype="multipart/form-data">

						@csrf
						@method('PUT')
						<fieldset>

							<legend>Session Details:</legend>

							<div class="row">

								<div class="row col-md-5">

									<div class="col-sm-12">

										<label>User<code>*</code></label>

										<select class="form-control" id="user_id" name="user_id">

											<option value="">--Select User--</option>

											@if(!empty($user))

											@foreach($user as $user_data)

											@php $selected_user = ""; @endphp

											@if($user_data->id == $session->user_id)

											@php $selected_user = "selected"; @endphp

											@endif

											<option {{$selected_user}} value="{{ $user_data->id }}">{{ $user_data->name }}{{ $user_data->lname }}</option>

											@endforeach

											@endif

										</select>

									</div>

									<div class="col-sm-12">

										<label>Subject Title<code>*</code></label>

										<input type="text" class="form-control" name="sub_title" id="sub_title" placeholder="Enter Subject Title" autocomplete="off" value="{{ $session->sub_title }}">

									</div>

								</div>



								<div class="col-sm-7">

									<label>Subject Description<code>*</code></label>

									<textarea class="form-control" name="sub_desc" id="sub_desc" rows="5">{{ $session->sub_desc }}</textarea>

								</div>

							</div>

							<div class="row">

								<div class="col-sm-4">

									<label>Upload Subject Thumbnail Image<code>*</code></label>

									<input type="file" class="form-control" name="sub_image" id="sub_image">

									<input type="hidden" name="hide_sub_image" id="hide_sub_image" value="{{ $session->sub_image }}">

								</div>

								<div class="col-sm-4">

									<label>Category<code>*</code></label>

									<select class="form-control" id="sub_category" name="sub_category" onchange="getSubcate()">

										<option value="">--Select Category--</option>

										@if(!empty($category))

										@foreach($category as $cat_data)

										@php $selected_cat = ""; @endphp

										@if($cat_data->id == $session->sub_category)

										@php $selected_cat = "selected"; @endphp

										<option {{$selected_cat}} value="{{ $cat_data->id }}">{{ $cat_data->name }}</option>

										@endif

										@endforeach

										@endif

									</select>

								</div>

								<div class="col-sm-4">

									<label>Sub Category<code>*</code></label>

									<select class="form-control" id="sub_subCategory" name="sub_subCategory">

										<option value="">--Select Sub Category--</option>

										@if(!empty($subCategory))

										@foreach($subCategory as $subcat_data)

										@php $selected_subcat = ""; @endphp

										@if($subcat_data->id == $session->sub_subCategory)

										@php $selected_subcat = "selected"; @endphp

										<option {{$selected_subcat}} value="{{ $subcat_data->id }}">{{ $subcat_data->sub_name }}</option>

										@endif

										@endforeach

										@endif

									</select>

								</div>

								<div class="col-sm-4">

									<label>Duration<code>*</code></label>

									<input type="text" class="form-control" name="sub_duration" id="sub_duration" placeholder="Enter Duration" autocomplete="off" value="{{ $session->sub_duration }}">

								</div>

								<div class="col-sm-4">

									<label>Type<code>*</code></label>

									<select class="form-control" id="sub_type" name="sub_type" onchange="typeChange()">

										@php $selected_type1 = ""; $selected_type2 = "" @endphp

										@if($session->sub_type == 1)

										@php $selected_type1 = "selected" @endphp

										@endif



										@if($session->sub_type == 2)

										@php $selected_type2 = "selected" @endphp

										@endif

										<option value="">--Select Type--</option>

										<option {{ $selected_type1 }} value="1">One To One</option>

										<option {{ $selected_type2 }} value="2">Gruop</option>

									</select>

								</div>

							</div>

						</fieldset><br/>



						@php $session_stype = "none" @endphp

						@if($session->sub_type == 1)

						@php $session_stype = "display" @endphp

						@endif
						<fieldset class="session_oneTo_one" style="display: {{ $session_stype }};">

							<legend>Session One To One Details:</legend>

							@if(!empty($SessionOneToOne))
							<input type="hidden" name="session_oto_id" value="{{ $SessionOneToOne->id }}">

							<div class="row">

								<div class="col-sm-8">

									<label>Session Title<code>*</code></label>

									<input type="text" class="form-control" name="session_title" id="session_title" placeholder="Enter Session Title" autocomplete="off" value="{{ $SessionOneToOne->session_title }}">

								</div>

								<div class="col-sm-2">

									<label>Per Hour Session Price<code>*</code></label>

									<input type="text" class="form-control" name="session_hr_price" id="session_hr_price" placeholder="Enter Per Hour Session Price" autocomplete="off" value="{{ $SessionOneToOne->session_hr_price }}">

								</div>

								<div class="col-sm-2">

									<label>Select Grade<code>*</code></label>

									<select class="form-control" id="session_grade" name="session_grade">

										@php $selected_grade1 = ""; $selected_grade2 = ""; $selected_grade3 = ""; $selected_grade4 = ""; $selected_grade5 = ""; @endphp

										@if($SessionOneToOne->session_grade == 'A')

										@php $selected_grade1 = "selected" @endphp

										@endif



										@if($SessionOneToOne->session_grade == 'B')

										@php $selected_grade2 = "selected" @endphp

										@endif



										@if($SessionOneToOne->session_grade == 'C')

										@php $selected_grade3 = "selected" @endphp

										@endif



										@if($SessionOneToOne->session_grade == 'D')

										@php $selected_grade4 = "selected" @endphp

										@endif



										@if($SessionOneToOne->session_grade == 'E')

										@php $selected_grade5 = "selected" @endphp

										@endif

										<option>--Select Grade--</option>

										<option {{ $selected_grade1 }} value="A">A</option>

										<option {{ $selected_grade2 }} value="B">B</option>

										<option {{ $selected_grade3 }} value="C">C</option>

										<option {{ $selected_grade4 }} value="D">D</option>

										<option {{ $selected_grade5 }} value="E">E</option>

									</select>

								</div>

							</div>
							@else
							<input type="hidden" name="session_oto_id" value="0">

							<div class="row">

								<div class="col-sm-8">

									<label>Session Title<code>*</code></label>

									<input type="text" class="form-control" name="session_title" id="session_title" placeholder="Enter Session Title" autocomplete="off" value="">

								</div>

								<div class="col-sm-2">

									<label>Per Hour Session Price<code>*</code></label>

									<input type="text" class="form-control" name="session_hr_price" id="session_hr_price" placeholder="Enter Per Hour Session Price" autocomplete="off" value="">

								</div>

								<div class="col-sm-2">

									<label>Select Grade<code>*</code></label>

									<select class="form-control" id="session_grade" name="session_grade">

										<option>--Select Grade--</option>

										<option value="A">A</option>

										<option value="B">B</option>

										<option value="C">C</option>

										<option value="D">D</option>

										<option value="E">E</option>

									</select>

								</div>

							</div>
							@endif
							<br/>

						</fieldset>

						



						@php $session_sty = "none" @endphp

						@if($session->sub_type == 2)

						@php $session_sty = "display" @endphp

						@endif

						<fieldset class="session_group" style="display: {{ $session_sty }};">

							<legend>Session Group Detail:</legend>



							@if(count($SessionGroup)) 

							<input type="hidden" name="session_count" id="session_count" value="{{ count($SessionGroup) }}">

							@endif



							@php $count_session = 0; @endphp

							@if(count($SessionGroup) > 0)

							@foreach($SessionGroup as $SessionGroup_data)

							@php $count_session = $count_session + 1; $session_class = "" @endphp

							@if($count_session == 1)

							@php $session_class = "session_group_div"; @endphp

							@endif

							<div class="row {{$session_class}}">

								<input type="hidden" id="session_grup_id" name="session_grup_id[]" value="{{ $SessionGroup_data->id }}">

								<div class="col-sm-3">

									<label>Session Title<code>*</code></label>

									<input type="text" class="form-control" name="session_grp_title[]" id="session_grp_title" placeholder="Enter Session Title" autocomplete="off" value="{{ $SessionGroup_data->session_grp_title }}">

								</div>

								<div class="col-sm-2">

									<label>Session Date Time<code>*</code></label>

									<input type="text" class="form-control session_grup_date_time" name="session_grup_date_time[]" id="session_grup_date_time1" placeholder="Enter Session Date Time" autocomplete="off" value="{{ $SessionGroup_data->session_grup_date_time }}">

								</div>

								<div class="col-sm-2">

									<label>Subject Fess<code>*</code></label>

									<input type="text" class="form-control" name="session_grup_fess[]" id="session_grup_fess" placeholder="Enter Subject Fees" autocomplete="off" value="{{ $SessionGroup_data->session_grup_fess }}">

								</div>

								<div class="col-sm-2">

									<label>No. of Attendees<code>*</code></label>

									<input type="text" class="form-control" name="session_grup_attendees[]" id="session_grup_attendees" placeholder="Enter No. of Attendees" autocomplete="off" value="{{ $SessionGroup_data->session_grup_attendees }}">

								</div>

								<div class="col-sm-2">

									<label>Select Grade<code>*</code></label>

									<select class="form-control" id="session_grup_grade" name="session_grup_grade[]">

										@php $sele_grd1 = ""; $sele_grd2 = ""; $sele_grd3 = ""; $sele_grd4 = ""; $sele_grd5 = ""; @endphp

										@if($SessionGroup_data->session_grup_grade == 'A')

										@php $sele_grd1 = "selected" @endphp

										@endif



										@if($SessionGroup_data->session_grup_grade == 'B')

										@php $sele_grd2 = "selected" @endphp

										@endif



										@if($SessionGroup_data->session_grup_grade == 'C')

										@php $sele_grd3 = "selected" @endphp

										@endif



										@if($SessionGroup_data->session_grup_grade == 'D')

										@php $sele_grd4 = "selected" @endphp

										@endif



										@if($SessionGroup_data->session_grup_grade == 'E')

										@php $sele_grd5 = "selected" @endphp

										@endif

										<option>--Select Grade--</option>

										<option {{ $sele_grd1 }} value="A">A</option>

										<option {{ $sele_grd2 }} value="B">B</option>

										<option {{ $sele_grd3 }} value="C">C</option>

										<option {{ $sele_grd4 }} value="D">D</option>

										<option {{ $sele_grd5 }} value="E">E</option>

									</select>

								</div>

								<div class="col-sm-1">

									<label></label>

									<div>

										<button title="Add Session" style="background: #968cbd;" type="button" class="add_more_session btn btn-sm">

											<i style="color: #ffffff" class="la la-plus"></i>

										</button>



										<button style="background-color: red;" title="Remove Session" type="button" class="remove_session btn btn-sm ">

											<i class="la la-minus" style="color: #ffffff"></i>

										</button>

									</div>

								</div>

							</div>

							@endforeach

							@else

							<div class="row session_group_div">
								<input type="hidden" id="session_grup_id" name="session_grup_id[]" value="0">

								<div class="col-sm-3">

									<label>Session Title<code>*</code></label>

									<input type="text" class="form-control" name="session_grp_title[]" id="session_grp_title" placeholder="Enter Session Title" autocomplete="off">

								</div>

								<div class="col-sm-2">

									<label>Session Date Time<code>*</code></label>

									<input type="text" class="form-control session_grup_date_time" name="session_grup_date_time[]" id="session_grup_date_time1" placeholder="Enter Session Date Time" autocomplete="off">

								</div>

								<div class="col-sm-2">

									<label>Subject Fess<code>*</code></label>

									<input type="text" class="form-control" name="session_grup_fess[]" id="session_grup_fess" placeholder="Enter Subject Fees" autocomplete="off">

								</div>

								<div class="col-sm-2">

									<label>No. of Attendees<code>*</code></label>

									<input type="text" class="form-control" name="session_grup_attendees[]" id="session_grup_attendees" placeholder="Enter No. of Attendees" autocomplete="off">

								</div>

								<div class="col-sm-2">

									<label>Select Grade<code>*</code></label>

									<select class="form-control" id="session_grup_grade" name="session_grup_grade[]">

										<option>--Select Grade--</option>

										<option value="A">A</option>

										<option value="B">B</option>

										<option value="C">C</option>

										<option value="D">D</option>

										<option value="E">E</option>

									</select>

								</div>

								<div class="col-sm-1">

									<label></label>

									<div>

										<button title="Add Session" style="background: #968cbd;" type="button" class="add_more_session btn btn-sm">

											<i style="color: #ffffff" class="la la-plus"></i>

										</button>



										<button style="background-color: red;" title="Remove Session" type="button" class="remove_session btn btn-sm ">

											<i class="la la-minus" style="color: #ffffff"></i>

										</button>

									</div>

								</div>

							</div>

							@endif

						</fieldset>



						<div class="kt-portlet__foot">

							<div class="kt-form__actions">

								<button type="button" class="btn btn-brand" id="save">Submit</button>

								<a href="{{route('session.index')}}" class="btn btn-dark">Cancel</a>

							</div>

						</div>



					</form>

				</div>

			</div>

		</div>

	</div>



</div>



<script>

	$(document).on('click','.remove_session',function(){

		var num_of_session_clone_div = $("#session_count").val();

		if (num_of_session_clone_div != 1) {

			session_count = $("#session_count").val();

			session_count = parseInt(session_count) - 1;

			$("#session_count").val(session_count);



			var obj = $(this).closest('.row');

			obj.remove();

		}

	});

	$('#sub_duration').timepicker({

		defaultTime: 'value',

		minuteStep: 1,

		disableFocus: true,

		template: 'dropdown',

		showMeridian:false

	});

	$('#session_grup_date_time1').datetimepicker({

		todayHighlight: true,

		autoclose: true,

		format: 'yyyy-mm-dd hh:ii'

	});



	function getSubcate(){

		id = $("#sub_category").val();



		$.ajax({

			type:'POST',

			url:"{{route('get.subCate')}}",

			data:{

				'_token' : $('input[name="_token"]').val(),

				'cat_id':id

			},

			success:function(data){

				$("#sub_subCategory").html();

				$("#sub_subCategory").html(data);

			}

		});

	}



	function typeChange() {

		var type = $("#sub_type").val();

		if(type == 1){
			
			$('.session_oneTo_one').show();

			$('.session_group').hide();

		}else if(type == 2){
			
			$('.session_oneTo_one').hide();

			$('.session_group').show();

		}else{
			
			$('.session_oneTo_one').hide();

			$('.session_group').hide();

		}

	}



	$(document).on('click','.add_more_session',function() {



		session_count = $("#session_count").val();

		session_count = parseInt(session_count) + 1;

		$("#session_count").val(session_count);

		

		var $row = $('.session_group_div'); 

		var $clone = $('.session_group_div').clone();

		$clone.find('#session_grup_id').val(0); 

		$clone.find('#session_grp_title').val(''); 

		$clone.find('#session_grup_fess').val('');

		$clone.find('#session_grup_attendees').val('');

		$clone.find('#session_grup_grade').val('');

		

		$clone.find('.session_grup_date_time').attr('id','session_grup_date_time'+session_count);

		$clone.closest('div').attr('class','row');



		$row.after($clone);



		$('#session_grup_date_time'+session_count).datetimepicker({

			todayHighlight: true,

			autoclose: true,

			format: 'yyyy-mm-dd hh:ii'

		});

	});



	// $(".edit__form").validate({

	// 	rules:

	// 	{

	// 		user_id:{required:true},

	// 		title:{required:true},

	// 		description:{required:true},

	// 		image:{required:true},

	// 		cat_id:{required:true},

	// 		subcat_id:{required:true},

	// 		duration:{required:true},

	// 		type:{required:true},

	// 		fees:{required:true}

	// 	},

	// 	messages:

	// 	{

	// 		user_id:{required:"Please select user"},

	// 		title:{required:"Please enter title"},

	// 		description:{required:"Please enter description"},

	// 		image:{required:"Please upload image"},

	// 		cat_id:{required:"Please select category"},

	// 		subcat_id:{required:"Please select sub category"},

	// 		duration:{required:"Please select duration"},

	// 		type:{required:"Please select type"},

	// 		fees:{required:"Please enter fees"}

	// 	}

	// });



	$("#save").on("click", function (e)

	{

		e.preventDefault();

		if ($(".edit__form").valid())

		{

			$.ajax({

				type: "POST",

				url: "{{ route('session.update', array($session->id)) }}",

				data: new FormData($('.edit__form')[0]),

				processData: false,

				contentType: false,

				success: function (data)

				{

					if (data.status === 'success') 

					{

						toastr.options.timeOut = 3000;

						toastr.options.fadeOut = 3000;

						toastr.options.progressBar = true;

						toastr.options.onHidden = function(){

							window.location = "{{ route('session.index') }}"

						};



						toastr["success"]("{{$title}} Added Successfully", "Success");

					}

					else if(data.status === 'error') 

					{

						toastr.options.timeOut = 3000;

						toastr.options.fadeOut = 3000;

						toastr.options.progressBar = true;



						toastr["error"]("Opps.. Something Went Wrong.!", "Error");

					}

				}

			});

		}

		else

		{

			e.preventDefault();

		}

	});

</script>

@stop