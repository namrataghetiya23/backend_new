@extends('main')

@section('content')



<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content"><br/>





	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

		<div class="kt-portlet kt-portlet--mobile">

			<div class="kt-portlet__head kt-portlet__head--lg">

				<div class="kt-portlet__head-label">

					<span class="kt-portlet__head-icon">

						<i class="kt-font-brand flaticon2-line-chart"></i>

					</span>

					<h3 class="kt-portlet__head-title">

						Add {{ $title }}

					</h3>

				</div>

			</div>

			<div class="kt-portlet__body">

				<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

					<form class="kt-form add__form" id="add__form" method="post" enctype="multipart/form-data">

						@csrf

						<fieldset>

							<legend>Session Details:</legend>

							<div class="row">

								<div class="row col-md-5">

									<div class="col-sm-12">

										<label>User<code>*</code></label>

										<select class="form-control" id="user_id" name="user_id">

											<option value="">--Select User--</option>

											@if(!empty($user))

											@foreach($user as $user_data)

											<option value="{{ $user_data->id }}">{{ $user_data->name }}{{ $user_data->lname }}</option>

											@endforeach

											@endif

										</select>

									</div>

									<div class="col-sm-12">

										<label>Subject Title<code>*</code></label>

										<input type="text" class="form-control" name="sub_title" id="sub_title" placeholder="Enter Subject Title" autocomplete="off">

									</div>

								</div>



								<div class="col-sm-7">

									<label>Subject Description<code>*</code></label>

									<textarea class="form-control" name="sub_desc" id="sub_desc" rows="5"></textarea>

								</div>

							</div>

							<div class="row">

								<div class="col-sm-4">

									<label>Upload Subject Thumbnail Image<code>*</code></label>

									<input type="file" class="form-control" name="sub_image" id="sub_image">

								</div>

								<div class="col-sm-4">

									<label>Category<code>*</code></label>

									<select class="form-control" id="sub_category" name="sub_category" onchange="getSubcate()">

										<option value="">--Select Category--</option>

										@if(!empty($category))

										@foreach($category as $cat_data)

										<option value="{{ $cat_data->id }}">{{ $cat_data->name }}</option>

										@endforeach

										@endif

									</select>

								</div>

								<div class="col-sm-4">

									<label>Sub Category<code>*</code></label>

									<select class="form-control" id="sub_subCategory" name="sub_subCategory">

										<option value="">--Select Sub Category--</option>

									</select>

								</div>

								<div class="col-sm-4">

									<label>Duration<code>*</code></label>

									<input type="text" class="form-control" name="sub_duration" id="sub_duration" placeholder="Enter Duration" autocomplete="off">

								</div>

								<div class="col-sm-4">

									<label>Type<code>*</code></label>

									<select class="form-control" id="sub_type" name="sub_type" onchange="typeChange()">

										<option value="">--Select Type--</option>

										<option value="1">One To One</option>

										<option value="2">Gruop</option>

									</select>

								</div>

							</div>

						</fieldset><br/>



						<fieldset class="session_oneTo_one" style="display: none;">

							<legend>Session One To One Details:</legend>



							<div class="row session_oneTo_one_div">

								<div class="col-sm-8">

									<label>Session Title<code>*</code></label>

									<input type="text" class="form-control" name="session_title" id="session_title" placeholder="Enter Session Title" autocomplete="off">

								</div>

								<div class="col-sm-2">

									<label>Per Hour Session Price<code>*</code></label>

									<input type="text" class="form-control" name="session_hr_price" id="session_hr_price" placeholder="Enter Per Hour Session Price" autocomplete="off">

								</div>

								<div class="col-sm-2">

									<label>Select Grade<code>*</code></label>

									<select class="form-control" id="session_grade" name="session_grade">

										<option>--Select Grade--</option>

										<option value="A">A</option>

										<option value="B">B</option>

										<option value="C">C</option>

										<option value="D">D</option>

										<option value="E">E</option>

									</select>

								</div>

							</div><br/>

						</fieldset>



						<fieldset class="session_group" style="display: none;">

							<legend>Session Group Detail:</legend>

							<input type="hidden" name="session_count" id="session_count" value="1">

							

							<div class="row session_group_div">

								<div class="col-sm-3">

									<label>Session Title<code>*</code></label>

									<input type="text" class="form-control" name="session_grp_title[]" id="session_grp_title" placeholder="Enter Session Title" autocomplete="off">

								</div>

								<div class="col-sm-2">

									<label>Session Date Time<code>*</code></label>

									<input type="text" class="form-control session_grup_date_time" name="session_grup_date_time[]" id="session_grup_date_time1" placeholder="Enter Session Date Time" autocomplete="off">

								</div>

								<div class="col-sm-2">

									<label>Subject Fess<code>*</code></label>

									<input type="text" class="form-control" name="session_grup_fess[]" id="session_grup_fess" placeholder="Enter Subject Fees" autocomplete="off">

								</div>

								<div class="col-sm-2">

									<label>No. of Attendees<code>*</code></label>

									<input type="text" class="form-control" name="session_grup_attendees[]" id="session_grup_attendees" placeholder="Enter No. of Attendees" autocomplete="off">

								</div>

								<div class="col-sm-2">

									<label>Select Grade<code>*</code></label>

									<select class="form-control" id="session_grup_grade" name="session_grup_grade[]">

										<option>--Select Grade--</option>

										<option value="A">A</option>

										<option value="B">B</option>

										<option value="C">C</option>

										<option value="D">D</option>

										<option value="E">E</option>

									</select>

								</div>

								<div class="col-sm-1">

									<label></label>

									<div>

										<button title="Add Session" style="background: #968cbd;" type="button" class="add_more_session btn btn-sm">

											<i style="color: #ffffff" class="la la-plus"></i>

										</button>



										<button style="background-color: red;" title="Remove Session" type="button" class="remove_session btn btn-sm ">

											<i class="la la-minus" style="color: #ffffff"></i>

										</button>

									</div>

								</div>

							</div>

						</fieldset>



						<div class="kt-portlet__foot">

							<div class="kt-form__actions">

								<button type="button" class="btn btn-brand" id="save">Submit</button>

								<a href="{{route('session.index')}}" class="btn btn-dark">Cancel</a>

							</div>

						</div>



					</form>

				</div>

			</div>

		</div>

	</div>



</div>



<script>

	$(document).on('click','.remove_session',function(){

		var num_of_session_clone_div = $("#session_count").val();

		if (num_of_session_clone_div != 1) {

			session_count = $("#session_count").val();

			session_count = parseInt(session_count) - 1;

			$("#session_count").val(session_count);



			var obj = $(this).closest('.row');

			obj.remove();

		}

	});


	$('#sub_duration').timepicker({

		defaultTime: 'value',

		minuteStep: 1,

		disableFocus: true,

		template: 'dropdown',

		showMeridian:false

	});

	$('#session_grup_date_time1').datetimepicker({

		todayHighlight: true,

		autoclose: true,

		format: 'yyyy-mm-dd hh:ii'

	});



	function getSubcate(){

		id = $("#sub_category").val();



		$.ajax({

			type:'POST',

			url:"{{route('get.subCate')}}",

			data:{

				'_token' : $('input[name="_token"]').val(),

				'cat_id':id

			},

			success:function(data){

				$("#sub_subCategory").html();

				$("#sub_subCategory").html(data);

			}

		});

	}



	function typeChange() {

		var type = $("#sub_type").val();

		if(type == 1){

			$('.session_oneTo_one').show();

			$('.session_group').hide();

		}else if(type == 2){

			$('.session_oneTo_one').hide();

			$('.session_group').show();

		}else{

			$('.session_oneTo_one').hide();

			$('.session_group').hide();

		}

	}



	$(document).on('click','.add_more_session',function() {



		session_count = $("#session_count").val();

		session_count = parseInt(session_count) + 1;

		$("#session_count").val(session_count);

		

		var $row = $('.session_group_div'); 

		var $clone = $('.session_group_div').clone();



		$clone.find('#session_grp_title').val(''); 

		$clone.find('#session_grup_fess').val('');

		$clone.find('#session_grup_attendees').val('');

		$clone.find('#session_grup_grade').val('');

		

		$clone.find('.session_grup_date_time').attr('id','session_grup_date_time'+session_count);

		$clone.closest('div').attr('class','row');



		$row.after($clone);



		$('#session_grup_date_time'+session_count).datetimepicker({

			todayHighlight: true,

			autoclose: true,

			format: 'yyyy-mm-dd hh:ii'

		});

	});



	// $(".add__form").validate({

	// 	rules:

	// 	{

	// 		user_id:{required:true},

	// 		title:{required:true},

	// 		description:{required:true},

	// 		image:{required:true},

	// 		cat_id:{required:true},

	// 		subcat_id:{required:true},

	// 		duration:{required:true},

	// 		type:{required:true},

	// 		fees:{required:true}

	// 	},

	// 	messages:

	// 	{

	// 		user_id:{required:"Please select user"},

	// 		title:{required:"Please enter title"},

	// 		description:{required:"Please enter description"},

	// 		image:{required:"Please upload image"},

	// 		cat_id:{required:"Please select category"},

	// 		subcat_id:{required:"Please select sub category"},

	// 		duration:{required:"Please select duration"},

	// 		type:{required:"Please select type"},

	// 		fees:{required:"Please enter fees"}

	// 	}

	// });



	$("#save").on("click", function (e)

	{

		e.preventDefault();

		if ($(".add__form").valid())

		{

			$.ajax({

				type: "POST",

				url: "{{ route('session.store') }}",

				data: new FormData($('.add__form')[0]),

				processData: false,

				contentType: false,

				success: function (data)

				{

					if (data.status === 'success') 

					{

						toastr.options.timeOut = 3000;

						toastr.options.fadeOut = 3000;

						toastr.options.progressBar = true;

						toastr.options.onHidden = function(){

							window.location = "{{ route('session.index') }}"

						};



						toastr["success"]("{{$title}} Added Successfully", "Success");

					}

					else if(data.status === 'error') 

					{

						toastr.options.timeOut = 3000;

						toastr.options.fadeOut = 3000;

						toastr.options.progressBar = true;



						toastr["error"]("Opps.. Something Went Wrong.!", "Error");

					}

				}

			});

		}

		else

		{

			e.preventDefault();

		}

	});

</script>

@stop