@extends('layouts.auth')

@section('content')

<!--begin::Content-->
<div class="kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper">

    <!--begin::Head-->
    <div class="kt-login__head">
        <!-- <span class="kt-login__signup-label">Don't have an account yet?</span>&nbsp;&nbsp; -->
        <!-- <a href="#" class="kt-link kt-login__signup-link">Sign Up!</a> -->
    </div>

    <!--end::Head-->

    <!--begin::Body-->
    <div class="kt-login__body">

        <!--begin::Signin-->
        <div class="kt-login__form">
            <div class="kt-login__title">
                <h3 style="color: #445f2194;margin-bottom: -2.5rem;font-weight: 800;">Sign Up</h3>
            </div>

            <!--begin::Form-->
            <form class="kt-form" method="POST" action="{{ route('register') }}" novalidate="novalidate" id="">
                @csrf
                <div class="form-group">
                    <input class="form-control @error('email') is-invalid @enderror" type="text" placeholder="Username" id="email" type="email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <input class="form-control @error('password') is-invalid @enderror" id="password" type="password" name="password" required autocomplete="current-password" placeholder="password">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <!--begin::Action-->
                <div class="kt-login__actions">
                    <a href="{{ route('login') }}" class="kt-link kt-login__link-forgot">
                        Log In ?
                    </a>
                    <button style="background: #445f2194;border-color: #445f2194; color: #fff" type="" id="" class="btn btn-elevate kt-login__btn-primary">Sign Up</button>
                </div>

                <!--end::Action-->
            </form>

            <!--end::Form-->

        </div>

        <!--end::Signin-->
    </div>

    <!--end::Body-->
</div>

<!--end::Content-->
</div>

@endsection
