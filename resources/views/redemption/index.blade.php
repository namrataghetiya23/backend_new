@extends('main')

@section('content')



<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content"><br/>

	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

		<div class="kt-portlet kt-portlet--mobile">

			<div class="kt-portlet__head kt-portlet__head--lg">

				<div class="kt-portlet__head-label">

					<span class="kt-portlet__head-icon">

						<i class="kt-font-brand flaticon2-line-chart"></i>

					</span>

					<h3 class="kt-portlet__head-title">

						{{ $title }} List

					</h3>

				</div>

			</div>

			<div class="kt-portlet__body">

				<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">



					<div class="row">

						<div class="col-sm-12" id="user_list_table">

							<table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="user_list" role="grid" aria-describedby="kt_table_1_info">

								@csrf



								<thead>

									<tr role="row">

										<th>No</th>

										<th>Username</th>

										<th>Requested Date</th>

										<th>Redemption Amount</th>

										<th>Pay to User</th>

										<th>Status</th>
									</tr>

								</thead>

								<tbody>


									<tr>

										<td>1</td>

										<td>Ainsley Valdez 12 Noel Duffy</td>

										<td>21 June,2020</td>

										<td>5000</td>

										<td>4000</td>

										<td>Pending</td>

									</tr>

									<tr>

										<td>2</td>

										<td>Amber Arnold tw Flynn Stanton</td>

										<td>21 June,2020</td>

										<td>2020</td>

										<td>1000</td>

										<td>Paid</td>

									</tr>

								</tbody>

							</table>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

<script>

	$(document).ready(function() {

		$('#user_list').DataTable();

	});

</script>

@stop