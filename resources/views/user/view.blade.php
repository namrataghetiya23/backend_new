@extends('main')
@section('content')

<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content"><br/>

	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="kt-font-brand flaticon2-line-chart"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						View {{ $title }}
					</h3>
				</div>
			</div>
			<div class="kt-portlet__body">
				<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
					<div class="row">
						<div class="col-sm-4">
							<label>Role<code>*</code></label>
							<input type="text" class="form-control" name="name" id="name" placeholder="Enter First Name" autocomplete="off" value="{{ $edit->title }}" disabled="disabled">
						</div>
						<div class="col-sm-4">
							<label>First Name<code>*</code></label>
							<input type="text" class="form-control" name="name" id="name" placeholder="Enter First Name" autocomplete="off" value="{{ $edit->name }}" disabled="disabled">
						</div>
						<div class="col-sm-4">
							<label>Last Name<code>*</code></label>
							<input type="text" class="form-control" name="lname" id="lname" placeholder="Enter Last Name" autocomplete="off" value="{{ $edit->lname }}" disabled="disabled">
						</div>
						<div class="col-sm-4">
							<label>Email<code>*</code></label>
							<input type="text" class="form-control" name="email" id="email" placeholder="Enter Email" autocomplete="off" value="{{ $edit->email }}" disabled="disabled">
						</div>
						<div class="col-sm-4">
							<label>Mobile No.<code>*</code></label>
							<input type="text" class="form-control" name="contact" id="contact" placeholder="Enter Mobile No." autocomplete="off" value="{{ $edit->contact }}" disabled="disabled">
						</div>
						<div class="col-sm-4">
							<label>School Name<code>*</code></label>
							<input type="text" class="form-control" name="school_name" id="school_name" placeholder="Enter School Name" autocomplete="off" value="{{ $edit->school_name }}" disabled="disabled">
						</div>
						<div class="col-sm-4">
							<label>Upload Documents</label>
							<input multiple type="file" class="form-control" name="document[]" id="document" placeholder="Upload Documents" accept=".xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf">
						</div>
					</div>
					<div class="kt-portlet__foot">
						<div class="kt-form__actions">
							<a href="{{route('user.index')}}" class="btn btn-dark">Back</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
@stop