@extends('main')
@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<br/>
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="kt-font-brand flaticon2-line-chart"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						{{ $title }} List
					</h3>
				</div>
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<div class="kt-portlet__head-actions">
							<a href="{{ route('course.create') }}" class="btn btn-brand btn-elevate btn-icon-sm">
								<i class="la la-plus"></i>
								Add {{$title}}
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="kt-portlet__body">
				<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
					<div class="row">
						<div class="col-sm-2">
							<div class="form-group">
								<select name="reqest" id="reqest" class="reqest form-control" onchange="changeCommision()">
									<option value="">--Select--</option>
									<option value="1">Commision</option>
									<option value="2">Cancle Charge Student</option>
									<option value="3">Cancle Charge Tutor</option>
								</select>
							</div>
						</div>
						<div class="col-sm-2 commission" style="display: none;">
							<div class="form-group">
								<input type="text" name="commission" id="commission" class="form-control">
							</div>
						</div>
						<div class="col-sm-3 chnageBtn_div" style="display: none;">
							<div class="form-group">
								<button type="button" class="btn btn-brand btn-elevate btn-icon-sm chnageBtn">Change Commision</button>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12" id="user_list_table">
							<table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="user_list" role="grid" aria-describedby="kt_table_1_info">
								@csrf
								<thead>
									<tr role="row">
										<th><input type="checkbox" class="checkboxAll" onchange="selectAll()"></th>
										<th>No</th>
										<th>Title</th>
										<th>Commision (%)</th>
										<th>Cancle Charge Student (%)</th>
										<th>Cancle Charge Tutor (%)</th>
										<th>Show On Home Slider</th>
										<th>Activate Status</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									@if(!empty($data))
									<?php $count = 1; ?>
									@foreach($data as $dt)
									<tr>
										<td><input type="checkbox" class="checkbox" onchange="changeCheckBox(this.id)" id="{{ $count }}"></td>
										<td>{{ $count++ }}</td>
										<td>{{ ucfirst($dt->title) }}</td>
										<td>{{ $dt->commission }}</td>
										<td>{{ $dt->cancle_charge_student }}</td>
										<td>{{ $dt->cancle_charge_tutor }}</td>
										<td>
											<span class="kt-switch kt-switch--sm">
												<label>
													<input type="checkbox" @if($dt->show_on_home == 1) checked @endif onchange="show_on_home_status('{{ $dt->id }}')" name="">
													<span></span>
												</label>
											</span>
										</td>
										<td>
											<span class="kt-switch kt-switch--sm">
												<label>
													<input type="checkbox" @if($dt->active == 1) checked @endif onchange="status('{{ $dt->id }}')" name="">
													<span></span>
												</label>
											</span>
										</td>
										<td>
											<!-- <a href="{{route('course.edit',$dt->id)}}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit" style="background: #968cbd;">
												<i class="la la-edit" style="color: white !important;"></i>
												
											</a> -->
											<input type="hidden" value="{{ $dt->id }}" name="delete_course_id" id="delete_course_id">
											<button class="btn btn-sm btn-clean btn-icon btn-icon-md" id="delete_course" style="background-color: red;" data-toggle="tooltip" title="Delete">
												<i class="la la-trash" style="color: #ffffff"></i>
											</button>
										</td>
									</tr>
									@endforeach
									@endif
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function() {

		$('#user_list').DataTable();

	});
	
</script>
<script>
	function selectAll() {
		var check = $('input[class="checkboxAll"]:checkbox:checked').length;
		if(check > 0){
			$('.checkbox').prop('checked', true);
		}else{
			$('.checkbox').prop('checked', false);
		}
	}
	
	function changeCommision() {
		var check = $('input[class="checkbox"]:checkbox:checked').length;
		console.log(check);
		if(check > 0){
			reqest = $("#reqest").val();
			if(reqest == 1){
				$(".commission").show();
				$("#commission").attr('placeholder','Commision');
				$(".chnageBtn").html("Chnage Commision");
				$(".chnageBtn_div").show();
			}
			else if(reqest == 2){
				$(".commission").show();
				$("#commission").attr('placeholder','Cancle Charge Tutor');
				$(".chnageBtn").html("Change Cancle Charge Tutor");
				$(".chnageBtn_div").show();
			}
			else if(reqest == 3){
				$(".commission").show();
				$("#commission").attr('placeholder','Cancle Charge Student');
				$(".chnageBtn").html("Change Cancle Charge Student");
				$(".chnageBtn_div").show();
			}
			else{
				$(".commission").hide();
				$(".chnageBtn_div").hide();	
			}
		}else{
			
			$(".chnageBtn_div").hide();
			$(".commission").hide();

			toastr.options.timeOut = 2000;
			toastr.options.fadeOut = 2000;
			toastr.options.progressBar = true;
			toastr.options.onHidden = function(){
				window.location = "{{ route('course.index') }}"
			};
			toastr["success"]("Please Select Checkbox", "Success");
		}
	}
	
	function changeCheckBox(id) {
		var check = $('input:checkbox:checked').length;
		$("#numberLabel").html("");
		$("#numberLabel").html(check +" row is selected");	
	}

	function show_on_home_status(id) {
		$.ajax({
			type:'POST',
			url:"{{route('course.homestatus')}}",
			data:{
				'_token' : $('input[name="_token"]').val(),
				'id':id
			},
			success:function(data){
				if (data.status === 'status_changed') 
				{
					toastr.options.timeOut = 1500;
					toastr.options.fadeOut = 1500;
					toastr.options.progressBar = true;
					toastr.options.onHidden = function(){
					};
					toastr["success"]("Show On Home Page Status Changed", "Success");
				}
			}
		});
	}
	
	function status(id){
		$.ajax({
			type:'POST',
			url:"{{route('course.status')}}",
			data:{
				'_token' : $('input[name="_token"]').val(),
				'id':id
			},
			success:function(data){
				if (data.status === 'status_changed') 
				{
					toastr.options.timeOut = 1500;
					toastr.options.fadeOut = 1500;
					toastr.options.progressBar = true;
					toastr.options.onHidden = function(){
					};
					toastr["success"]("Activation Status Changed", "Success");
				}
			}
		});
	}
	
	$(document).ready(function() {

		$(document).on('click', '.chnageBtn', function ()
		{
			idArray = [];
			$('input[class="checkbox"]:checkbox:checked').each(function(){
				idArray.push($(this).attr('id'));
			});

			reqest = $("#reqest").val();
			console.log(reqest);
			value = $("#commission").val();
			
			$.ajax({

				type: "post",

				url: '{{ route('course.changeCommision') }}',

				data: {
					'_token': $('input[name="_token"]').val(),
					'reqest': reqest,
					'value': value,
					'idArray': idArray
				},

				cache: false,

				success: function (data)

				{
					if (data.status === 'success') 
					{
						toastr.options.timeOut = 3000;
						toastr.options.fadeOut = 3000;
						toastr.options.progressBar = true;
						toastr.options.onHidden = function(){
							window.location = "{{ route('course.index') }}"
						};

						toastr["success"]("Updated Successfully", "Success");
					}
					else if(data.status === 'error') 
					{
						toastr.options.timeOut = 3000;
						toastr.options.fadeOut = 3000;
						toastr.options.progressBar = true;

						toastr["error"]("Opps.. Something Went Wrong.!", "Error");
					}
				}

			});

		});

		$(document).on('click', '#delete_course', function ()

		{

			var obj = $(this);

			var id = $(this).closest('td').find("#delete_course_id").val();



			swal({

				title: "Are you sure?",

				text: "You will not be able recover this record",

				type: "warning",

				showCancelButton: true,

				confirmButtonColor: "#DD6B55",

				confirmButtonText: "Yes delete it!",

				closeOnConfirm: false

			},

			function () {

				$.ajax({

					type: "post",

					url: '{{ route('course.delete') }}',

					data: {

						'_token': $('input[name="_token"]').val(),

						'id': id

					},

					cache: false,

					success: function (data)

					{

						obj.closest('tr').remove();

					}

				});

				swal("Deleted", "{{ $title }} has been deleted.", "success");

			});

		});

	});
	
	
	
</script>
@stop