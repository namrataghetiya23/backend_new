@extends('main')
@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<br/>
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="kt-font-brand flaticon2-line-chart"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						Add {{ $title }}
					</h3>
				</div>
			</div>
			<div class="kt-portlet__body">
				<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
					<form class="kt-form add__form" id="add__form" method="post" enctype="multipart/form-data">
						@csrf
						<fieldset>
							<legend>Course Details:</legend>
							<div class="row">
								<div class="col-sm-4">
									<label>User<code>*</code></label>
									<select class="form-control" id="user_id" name="user_id" required="required">
										<option value="">--Select User--</option>
										@if(!empty($user))
										@foreach($user as $user_data)
										<option value="{{ $user_data->id }}">{{ $user_data->name }}{{ $user_data->lname }}</option>
										@endforeach
										@endif
									</select>
								</div>
								<div class="col-sm-4">
									<label>title<code>*</code></label>
									<input type="text" class="form-control" name="title" id="title" placeholder="Enter Title" autocomplete="off" required="required">
								</div>
								<div class="col-sm-4">
									<label>Sub Title<code>*</code></label>
									<input type="text" name="sub_title" id="sub_title" class="form-control" placeholder="Enter Sub Title" required="required">
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<label>Upload Course Thumbnail Image<code>*</code></label>
									<input type="file" class="form-control" name="image" id="image" required="required">
								</div>
								<div class="col-sm-4">
									<label>Category<code>*</code></label>
									<select class="form-control" id="cat_id" name="cat_id" onchange="getSubcate()" required="required">
										<option value="">--Select Category--</option>
										@if(!empty($category))
										@foreach($category as $cat_data)
										<option value="{{ $cat_data->id }}">{{ $cat_data->name }}</option>
										@endforeach
										@endif
									</select>
								</div>
								<div class="col-sm-4">
									<label>Sub Category<code>*</code></label>
									<select class="form-control" id="subcat_id" name="subcat_id" required="required">
										<option value="">--Select Sub Category--</option>
									</select>
								</div>
								<div class="col-sm-4">
									<label>Commission<code>*</code></label>
									<input type="text" class="form-control" name="commission" id="commission" placeholder="Enter Commission" autocomplete="off" required="required">
								</div>
								<div class="col-sm-4">
									<label>Cancle Charge Student<code>*</code></label>
									<input type="text" class="form-control" name="cancle_charge_student" id="cancle_charge_student" placeholder="Enter Cancle Charge Student" autocomplete="off" required="required">
								</div>
								<div class="col-sm-4">
									<label>Cancle Charge Tutor<code>*</code></label>
									<input type="text" class="form-control" name="cancle_charge_tutor" id="cancle_charge_tutor" placeholder="Enter Cancle Charge Tutor" autocomplete="off" required="required">
								</div>
								<div class="col-sm-4">
									<label>Documents Upload</label>
									<input type="file" class="form-control" name="documents[]" id="documents" multiple="multiple">
								</div>
							</div>
						</fieldset>
						<fieldset class="batch_div batch_clone_div">
							<legend>Batch Deatils:</legend>
							<input type="hidden" name="batch_count" id="batch_count" value="1">
							<div class="row first_div">
								<div class="col-sm-3">
									<label>No. of Attendees<code>*</code></label>
									<input type="text" class="form-control no_of_attendees" name="no_of_attendees[]" id="no_of_attendees1" placeholder="Enter No. of Attendees" autocomplete="off" required="required">
								</div>
								<div class="col-sm-3">
									<label>Course Fees<code>*</code></label>
									<input type="text" class="form-control course_fess" name="course_fess[]" id="course_fess1" placeholder="Enter Course Fess" autocomplete="off" required="required">
								</div>
								<div class="col-sm-3">
									<label>Age Group<code>*</code></label>
									<input type="text" class="form-control age_group" name="age_group[]" id="age_group1" placeholder="Enter Age Group" autocomplete="off" required="required">
								</div>
								<div class="col-sm-3">
									<label></label>
									<div>
										<button title="Add Batch" style="background: #968cbd;" type="button" class="add_more_batch btn btn-sm">
											<i style="color: #ffffff" class="la la-plus"> Add Batch</i>
										</button>
										<button style="background-color: red; display: none;" title="Remove Batch" type="button" class="remove_batch btn btn-sm">
											<i class="la la-minus" style="color: #ffffff"> Remove Batch</i>
										</button>
									</div>
								</div>
								<div class="col-sm-12">
									<h5>Session Details</h5>
								</div>
								<input type="hidden" id="session_count_1" name="session_count[]" class="session_count" value="1">
								<div class="row col-sm-12 session_div" id="session_div_1_1">
									<div class="col-sm-3">
										<label>Session Date and Time<code>*</code></label>
										<input type="text" class="form-control session_dt_time" name="session_dt_time_1[]" id="session_dt_time_1_1" placeholder="Enter Session Date And Time" autocomplete="off" required="required">
									</div>
									<div class="col-md-3">
										<label>Session Type<code>*</code></label>
										<select class="form-control session_type" id="session_type_1_1" name="session_type_1[]" required="required">
											<option value="">--Select Type--</option>
											<option value="1">One To One</option>
											<option value="2">Group</option>
										</select>
									</div>
									<div class="col-sm-2">
										<label></label><br/>
										<input type="checkbox" class="recursive" id="recursive_1_1" name="recursive_1[]" value="recursive">
										<label for="recursive"> It is recursive </label>
									</div>
									<div class="col-sm-3 from_date_div" id="from_date_div_1_1" style="display: none;">
										<label>From Date<code>*</code></label>
										<input type="text" class="form-control from_date" name="from_date_1[]" id="from_date_1_1" placeholder="Enter From Date" autocomplete="off">
									</div>
									<div class="col-sm-3 to_date_div" id="to_date_div_1_1" style="display: none;">
										<label>To Date<code>*</code></label>
										<input type="text" class="form-control to_date" name="to_date_1[]" id="to_date_1_1" placeholder="Enter To Date" autocomplete="off">
									</div>
									<div class="col-sm-3 days_div" id="days_div_1_1" style="display: none;">
										<label>Sekect Days<code>*</code></label>
										<select class="form-control days" id="days_1_1" name="days_1[]" multiple="multiple">
											<option>Monday</option>
											<option>Tuesday</option>
											<option>Wednesday</option>
											<option>Thursday</option>
											<option>Friday</option>
											<option>Saturday</option>
											<option>Sunday</option>
										</select>
									</div>
									<div class="col-sm-3">
										<label></label>
										<div>
											<button title="Add Session" style="background: #968cbd;" type="button" id="add_more_session_1_1" class="add_more_session btn btn-sm" >
												<i style="color: #ffffff" class="la la-plus"> Add Session</i>
											</button>
											<button style="background-color: red; display: none;" title="Remove Session" type="button" id="remove_session_1_1" class="remove_session btn btn-sm ">
												<i class="la la-minus" style="color: #ffffff"> Remove Session</i>
											</button>
										</div>
									</div>
								</div>
							</div>
						</fieldset>
						<br/>
						<div class="kt-portlet__foot">
							<div class="kt-form__actions">
								<button type="button" class="btn btn-brand" id="save">Submit</button>
								<a href="{{route('course.index')}}" class="btn btn-dark">Cancel</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$('.session_dt_time').datetimepicker({
		todayHighlight: true,
		autoclose: true,
		format: 'yyyy-mm-dd h:i:s'
	});

	$('.from_date').datepicker({
		todayHighlight: true,
		autoclose: true,
		format: 'yyyy-mm-dd'
	});

	$('.to_date').datepicker({
		todayHighlight: true,
		autoclose: true,
		format: 'yyyy-mm-dd'
	});

	$(document).on('click','.add_more_batch',function() {

		batch_count = $("#batch_count").val();
		batch_count = parseInt(batch_count) + 1;
		$("#batch_count").val(batch_count);

		var $row = $('.batch_clone_div'); 
		var $clone = $('.batch_clone_div').clone();
		$clone.find('.first_div').parent().attr('class',"");
		$row.after($clone);

		cnt_session = $("#session_count_1").val();
		if(cnt_session > 1){
			for (var i = 2; i <= cnt_session ; i++) {
				$clone.find("#session_div_1_"+i).remove();
			}
		}

		$clone.find('.add_more_batch').remove();
		$clone.find('.no_of_attendees').attr('id','no_of_attendees'+batch_count);
		$clone.find('.course_fess').attr('id','course_fess'+batch_count);
		$clone.find('.age_group').attr('id','age_group'+batch_count);
		$clone.find('.session_dt_time').attr('id','session_dt_time_'+batch_count+"_1");
		$clone.find('.session_type').attr('id','session_type_'+batch_count+"_1");
		$clone.find('.recursive').attr('id','recursive_'+batch_count+"_1");
		$clone.find('.from_date').attr('id','from_date_'+batch_count+"_1");
		$clone.find('.to_date').attr('id','to_date_'+batch_count+"_1");
		$clone.find('.days').attr('id','days_'+batch_count+"_1");
		$clone.find('.add_more_session').attr('id','add_more_session_'+batch_count+"_1");
		$clone.find('.remove_session').attr('id','remove_session_'+batch_count+"_1");
		$clone.find('.session_div').attr('id','session_div_'+batch_count+"_1");
		$clone.find('.session_count').attr('id','session_count_'+batch_count);

		$clone.find('.from_date_div').attr('id','from_date_div_'+batch_count+"_1");
		$clone.find('.to_date_div').attr('id','to_date_div_'+batch_count+"_1");
		$clone.find('.days_div').attr('id','days_div_'+batch_count+"_1");

		$clone.find('.session_dt_time').attr('name','session_dt_time_'+batch_count+'[]');
		$clone.find('.session_type').attr('name','session_type_'+batch_count+'[]');
		$clone.find('.recursive').attr('name','recursive_'+batch_count+'[]');
		$clone.find('.from_date').attr('name','from_date_'+batch_count+'[]');
		$clone.find('.to_date').attr('name','to_date_'+batch_count+'[]');
		$clone.find('.days').attr('name','days_'+batch_count+'[]');

		$clone.find('.no_of_attendees').val('');
		$clone.find('.course_fess').val('');
		$clone.find('.age_group').val('');
		$clone.find('.session_dt_time').val('');
		$clone.find('.session_type').val('');
		$clone.find('.recursive').prop('checked', false);
		$clone.find('.from_date').val('');
		$clone.find('.to_date').val('');
		$clone.find('.days').val('');
		$clone.find('.session_count').val('1');
		$clone.find('.remove_batch').show();

		$('.session_dt_time').datetimepicker({
			todayHighlight: true,
			autoclose: true,
			format: 'yyyy-mm-dd h:i:s'
		});

		$('.from_date').datepicker({
			todayHighlight: true,
			autoclose: true,
			format: 'yyyy-mm-dd'
		});

		$('.to_date').datepicker({
			todayHighlight: true,
			autoclose: true,
			format: 'yyyy-mm-dd'
		});

	});

	$(document).on('click','.add_more_session',function() {
		id = $(this).attr('id');
		id = id.substr(id.length - 3);
		split_id = id.split("_");
		batch_number = split_id[0];

		session_number = $("#session_count_"+batch_number).val();
		add_session_number = parseInt(session_number) + 1;
		$("#session_count_"+batch_number).val(add_session_number);

		var $row = $('#session_div_'+batch_number+"_"+session_number);
		var $clone = $('#session_div_'+batch_number+"_"+session_number).clone().attr('id','session_div_'+batch_number+"_"+add_session_number);
		$row.after($clone);

		$clone.find('.session_dt_time').attr('id','session_dt_time_'+batch_number+"_"+add_session_number);
		$clone.find('.session_type').attr('id','session_type_'+batch_number+"_"+add_session_number);
		$clone.find('.recursive').attr('id','recursive_'+batch_number+"_"+add_session_number);
		$clone.find('.from_date').attr('id','from_date_'+batch_number+"_"+add_session_number);
		$clone.find('.to_date').attr('id','to_date_'+batch_number+"_"+add_session_number);
		$clone.find('.days').attr('id','days_'+batch_number+"_"+add_session_number);
		$clone.find('.add_more_session').attr('id','add_more_session_'+batch_number+"_"+add_session_number);
		$clone.find('.remove_session').attr('id','remove_session_'+batch_number+"_"+add_session_number);

		$clone.find('.add_more_session').hide();
		$clone.find('.remove_session').show();

		$clone.find('.from_date_div').attr('id','from_date_div_'+batch_number+"_"+add_session_number);
		$clone.find('.to_date_div').attr('id','to_date_div_'+batch_number+"_"+add_session_number);
		$clone.find('.days_div').attr('id','days_div_'+batch_number+"_"+add_session_number);

		$clone.find('.from_date_div').hide();
		$clone.find('.to_date_div').hide();
		$clone.find('.days_div').hide();

		$clone.find('.session_dt_time').val('');
		$clone.find('.session_type').val('');
		$clone.find('.recursive').prop('checked', false);
		$clone.find('.from_date').val('');
		$clone.find('.to_date').val('');
		$clone.find('.days').val('');

		$('.session_dt_time').datetimepicker({
			todayHighlight: true,
			autoclose: true,
			format: 'yyyy-mm-dd h:i:s'
		});

		$('.from_date').datepicker({
			todayHighlight: true,
			autoclose: true,
			format: 'yyyy-mm-dd'
		});

		$('.to_date').datepicker({
			todayHighlight: true,
			autoclose: true,
			format: 'yyyy-mm-dd'
		});
	});

	$(document).on('click','.recursive',function() {
		id = $(this).attr('id');
		id = id.substr(id.length - 3);
		split_id = id.split("_");
		batch_number = split_id[0];
		session_number = split_id[1];

		if ($(this).prop('checked')==true){ 
			$("#from_date_div_"+batch_number+"_"+session_number).show();
			$("#to_date_div_"+batch_number+"_"+session_number).show();
			$("#days_div_"+batch_number+"_"+session_number).show();
		}else{
			$("#from_date_div_"+batch_number+"_"+session_number).hide();
			$("#to_date_div_"+batch_number+"_"+session_number).hide();
			$("#days_div_"+batch_number+"_"+session_number).hide();
		}
	});	

	$(document).on('click','.remove_session',function(){
		id = $(this).attr('id');
		id = id.substr(id.length - 3);
		split_id = id.split("_");
		batch_number = split_id[0];

		session_number = $("#session_count_"+batch_number).val();
		session_number = session_number + 1;
		$("#session_count_"+batch_number).val(session_number);

		if (session_number != 1) {
			var obj = $(this).closest('.row');
			obj.remove();
		}

	});		

	$(document).on('click','.remove_batch',function(){
		var num_of_batch_clone_div = $("#batch_count").val();
		if (num_of_batch_clone_div != 1) {
			batch_count = $("#batch_count").val();
			batch_count = parseInt(batch_count) - 1;
			$("#batch_count").val(batch_count);
			var obj = $(this).closest('.row');
			obj.remove();
		}
	});

	function getSubcate(){
		id = $("#cat_id").val();
		$.ajax({
			type:'POST',
			url:"{{route('get.subCate')}}",
			data:{
				'_token' : $('input[name="_token"]').val(),
				'cat_id':id
			},
			success:function(data){
				$("#subcat_id").html();
				$("#subcat_id").html(data);
			}
		});
	}

	$("#save").on("click", function (e)
	{
		e.preventDefault();
		if ($(".add__form").valid())
		{
			$.ajax({
				type: "POST",
				url: "{{ route('course.store') }}",
				data: new FormData($('.add__form')[0]),
				processData: false,
				contentType: false,
				success: function (data)
				{
					toastr.options.timeOut = 3000;
					toastr.options.fadeOut = 3000;
					toastr.options.progressBar = true;
					toastr.options.onHidden = function(){
						window.location = "{{ route('course.index') }}"
					};

					toastr["success"]("{{$title}} Added Successfully", "Success");
					// if (data.status === 'success') 
					// {
						
					// }

					// else if(data.status === 'error') 
					// {
					// 	toastr.options.timeOut = 3000;
					// 	toastr.options.fadeOut = 3000;
					// 	toastr.options.progressBar = true;
					// 	toastr["error"]("Opps.. Something Went Wrong.!", "Error");
					// }
				}
			});
		}
		else
		{
			e.preventDefault();
		}
	});

</script>
@stop