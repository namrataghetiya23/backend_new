@extends('main')

@section('content')



<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content"><br/>





	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

		<div class="kt-portlet kt-portlet--mobile">

			<div class="kt-portlet__head kt-portlet__head--lg">

				<div class="kt-portlet__head-label">

					<span class="kt-portlet__head-icon">

						<i class="kt-font-brand flaticon2-line-chart"></i>

					</span>

					<h3 class="kt-portlet__head-title">

						Edit {{ $title }}

					</h3>

				</div>

			</div>

			<div class="kt-portlet__body">

				<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

					<form class="kt-form edit__form" id="edit__form" method="post" enctype="multipart/form-data">

						@csrf

						@method('PUT')

						<fieldset>

							<legend>Course Details:</legend>

							<div class="row">

								<div class="row col-md-5">

									<div class="col-sm-12">

										<label>User<code>*</code></label>

										<select class="form-control" id="user_id" name="user_id">

											<option value="">--Select User--</option>

											@if(!empty($user))

											@foreach($user as $user_data)

											@php $selected_user = ""; @endphp

											@if($user_data->id == $course->user_id)

											@php $selected_user = "selected"; @endphp

											@endif

											<option {{$selected_user}} value="{{ $user_data->id }}">{{ $user_data->name }}{{ $user_data->lname }}</option>

											@endforeach

											@endif

										</select>

									</div>

									<div class="col-sm-12">

										<label>title<code>*</code></label>

										<input type="text" class="form-control" name="title" id="title" placeholder="Enter Title" autocomplete="off" value="{{ $course->title }}">

									</div>

								</div>



								<div class="col-sm-7">

									<label>Description<code>*</code></label>

									<textarea class="form-control" name="description" id="description" rows="5">{{ $course->description }}</textarea>

								</div>

							</div>

							<div class="row">

								<div class="col-sm-4">

									<label>Upload Course Thumbnail Image<code>*</code></label>

									<input type="file" class="form-control" name="image" id="image">

									<input type="hidden" name="hide_image" id="hide_image" value="{{ $course->image }}">

								</div>

								<div class="col-sm-4">

									<label>Category<code>*</code></label>

									<select class="form-control" id="cat_id" name="cat_id" onchange="getSubcate()">

										<option value="">--Select Category--</option>

										@if(!empty($category))

										@foreach($category as $cat_data)

										@php $selected_cat = ""; @endphp

										@if($cat_data->id == $course->cat_id)

										@php $selected_cat = "selected"; @endphp

										<option {{$selected_cat}} value="{{ $cat_data->id }}">{{ $cat_data->name }}</option>

										@endif

										@endforeach

										@endif

									</select>

								</div>

								<div class="col-sm-4">

									<label>Sub Category<code>*</code></label>

									<select class="form-control" id="subcat_id" name="subcat_id">

										<option value="">--Select Sub Category--</option>

										@if(!empty($subCategory))

										@foreach($subCategory as $subcat_data)

										@php $selected_subcat = ""; @endphp

										@if($subcat_data->id == $course->subcat_id)

										@php $selected_subcat = "selected"; @endphp

										<option {{$selected_subcat}} value="{{ $subcat_data->id }}">{{ $subcat_data->sub_name }}</option>

										@endif

										@endforeach

										@endif

									</select>

								</div>

								<div class="col-sm-4">

									<label>Duration<code>*</code></label>

									<input type="text" class="form-control" name="duration" id="duration" placeholder="Enter Duration" autocomplete="off" value="{{ $course->duration }}">

								</div>

								<div class="col-sm-4">

									<label>Type<code>*</code></label>

									<select class="form-control" id="type" name="type" onchange="typeChange()">

										<option value="">--Select Type--</option>

										@php $selected_type1 = ""; $selected_type2 = ""; @endphp

										@if($course->type == 1)

										@php $selected_type1 = "selected" @endphp

										@endif

										@if($course->type == 2)

										@php $selected_type2 = "selected" @endphp

										@endif

										<option {{ $selected_type1 }} value="1">Live Session</option>

										<option {{ $selected_type2 }} value="2">Recorded</option>

									</select>

								</div>

								<div class="col-sm-4">

									<label>Fees<code>*</code></label>

									<input type="text" class="form-control" name="fees" id="fees" placeholder="Enter Fees" autocomplete="off" value="{{ $course->fees }}">

								</div>

							</div>

						</fieldset><br/>



						@php $session_stype = "none" @endphp

						@if($course->type == 1)

						@php $session_stype = "display" @endphp

						@endif

						<fieldset class="session_clone" style="display: {{ $session_stype }};">

							<legend>Session Details:</legend>

							

							@if(count($CourseSession)) 

							<input type="hidden" name="session_count" id="session_count" value="{{ count($CourseSession) }}">

							@endif

							

							@php $count_session = 0; @endphp

							@if(count($CourseSession) > 0)

							@foreach($CourseSession as $CourseSession_data)

							@php $count_session = $count_session + 1; $session_class = "" @endphp

							@if($count_session == 1)

							@php $session_class = "session_clone_div"; @endphp

							@endif

							<div class="row {{$session_class}}">

								<input type="hidden" name="session_id[]" id="session_id" value="{{ $CourseSession_data->id }}">

								<div class="col-sm-5">

									<label>Session Title<code>*</code></label>

									<input required="required" type="text" class="form-control" name="session_title[]" id="session_title" placeholder="Enter Session Title" autocomplete="off" value="{{ $CourseSession_data->session_title }}">

								</div>

								<div class="col-sm-3">

									<label>Session Date Time<code>*</code></label>

									<input required="required" type="text" class="form-control session_date_time" name="session_date_time[]" id="session_date_time1" placeholder="Enter Session Date Time" autocomplete="off" value="{{ $CourseSession_data->session_date_time }}">

								</div>

								<div class="col-sm-3">

									<label>No. of Attendees<code>*</code></label>

									<input required="required" type="text" class="form-control" name="session_attendees[]" id="session_attendees" placeholder="Enter No. of Attendees" autocomplete="off" value="{{ $CourseSession_data->session_attendees }}">

								</div>

								<div class="col-sm-1">

									<label></label>

									<div>

										<button title="Add Session" style="background: #968cbd;" type="button" class="add_more_session btn btn-sm">

											<i style="color: #ffffff" class="la la-plus"></i>

										</button>



										<button style="background-color: red;" title="Remove Session" type="button" class="remove_session btn btn-sm ">

											<i class="la la-minus" style="color: #ffffff"></i>

										</button>

									</div>

								</div>

							</div>

							@endforeach

							@else

							<div class="row session_clone_div">

								<div class="col-sm-5">

									<label>Session Title<code>*</code></label>

									<input required="required" type="text" class="form-control" name="session_title[]" id="session_title" placeholder="Enter Session Title" autocomplete="off">

								</div>

								<div class="col-sm-3">

									<label>Session Date Time<code>*</code></label>

									<input required="required" type="text" class="form-control session_date_time" name="session_date_time[]" id="session_date_time1" placeholder="Enter Session Date Time" autocomplete="off">

								</div>

								<div class="col-sm-3">

									<label>No. of Attendees<code>*</code></label>

									<input required="required" type="text" class="form-control" name="session_attendees[]" id="session_attendees" placeholder="Enter No. of Attendees" autocomplete="off">

								</div>

								<div class="col-sm-1">

									<label></label>

									<div>

										<button title="Add Session" style="background: #968cbd;" type="button" class="add_more_session btn btn-sm">

											<i style="color: #ffffff" class="la la-plus"></i>

										</button>



										<button style="background-color: red;" title="Remove Session" type="button" class="remove_session btn btn-sm ">

											<i class="la la-minus" style="color: #ffffff"></i>

										</button>

									</div>

								</div>

							</div>

							@endif

						</fieldset><br/>



						@php $chapter_stype = "none" @endphp

						@if($course->type == 2)

						@php $chapter_stype = "display" @endphp

						@endif

						<fieldset class="chapter_clone" style="display: {{ $chapter_stype }};">

							<legend>Chapter Details:</legend>

							@if(count($CourseChapter)) 

							<input type="hidden" name="chapter_count" id="chapter_count" value="1">

							@endif

							@php $count_chapter = 0; @endphp

							@if(count($CourseChapter) > 0)

							@foreach($CourseChapter as $CourseChapter_data)

							@php $count_chapter = $count_chapter + 1; $session_class = ""; @endphp

							@if($count_chapter == 1)

							@php $session_class = "chapter_clone_div"; @endphp

							@endif

							<div class="row {{ $session_class }}">

								<input type="hidden" name="chapter_id[]" id="chapter_id" value="{{ $CourseChapter_data->id }}">

								<div class="col-sm-3">

									<label>Chapter Title<code>*</code></label>

									<input type="text" class="form-control" name="chapter_title[]" id="chapter_title" placeholder="Enter Chapter Title" autocomplete="off" value="{{ $CourseChapter_data->chapter_title }}">

								</div>

								<div class="col-sm-4">

									<label>Chapter Description<code>*</code></label>

									<textarea class="form-control" id="chapter_description" name="chapter_description[]" rows="3">{{ $CourseChapter_data->chapter_description }}</textarea>

								</div>

								<div class="col-sm-2">

									<label>Upload Document<code>*</code></label>

									<input type="file" class="form-control" name="chapter_document[]" id="chapter_document">
									
									@if(!empty($CourseChapter_data->chapter_document))
									<a target="_blank" href="{{ asset('/course_chapter_img/'.$CourseChapter_data->chapter_document) }}"></a>
									@endif

									<input type="hidden" name="old_chapter_document[]" id="old_chapter_document" value="{{ $CourseChapter_data->chapter_document }}">

								</div>

								<div class="col-sm-2">

									<label>Upload Chapter Video<code>*</code></label>

									<input type="file" class="form-control" name="chapter_video[]" id="chapter_video">
									
									@if(!empty($CourseChapter_data->chapter_video))
									<a target="_blank" href="{{ asset('/course_chapter_video/'.$CourseChapter_data->chapter_video) }}"></a>
									@endif
									
									<input type="hidden" name="old_chapter_video[]" id="old_chapter_video" value="{{ $CourseChapter_data->chapter_video }}">
									
								</div>

								<div class="col-sm-1">

									<label></label>

									<div>

										<button title="Add Session" style="background: #968cbd;" type="button" class="add_more_chapter btn btn-sm">

											<i style="color: #ffffff" class="la la-plus"></i>

										</button>

										

										<button style="background-color: red;" title="Remove Session" type="button" class="remove_chapter btn btn-sm ">

											<i class="la la-minus" style="color: #ffffff"></i>

										</button>

									</div>

								</div>

							</div>

							@endforeach

							@else

							<div class="row chapter_clone_div">

								<div class="col-sm-3">

									<label>Chapter Title<code>*</code></label>

									<input type="text" class="form-control" name="chapter_title[]" id="chapter_title" placeholder="Enter Chapter Title" autocomplete="off">

								</div>

								<div class="col-sm-4">

									<label>Chapter Description<code>*</code></label>

									<textarea class="form-control" id="chapter_description" name="chapter_description[]" rows="3"></textarea>

								</div>

								<div class="col-sm-2">

									<label>Upload Document<code>*</code></label>

									<input type="file" class="form-control" name="chapter_document[]" id="chapter_document">

								</div>

								<div class="col-sm-2">

									<label>Upload Chapter Video<code>*</code></label>

									<input type="file" class="form-control" name="chapter_video[]" id="chapter_video">

								</div>

								<div class="col-sm-1">

									<label></label>

									<div>

										<button title="Add Session" style="background: #968cbd;" type="button" class="add_more_chapter btn btn-sm">

											<i style="color: #ffffff" class="la la-plus"></i>

										</button>

										

										<button style="background-color: red;" title="Remove Session" type="button" class="remove_chapter btn btn-sm ">

											<i class="la la-minus" style="color: #ffffff"></i>

										</button>

									</div>

								</div>

							</div>

							@endif

						</fieldset>



						<div class="kt-portlet__foot">

							<div class="kt-form__actions">

								<button type="button" class="btn btn-brand" id="save">Submit</button>

								<a href="{{route('course.index')}}" class="btn btn-dark">Cancel</a>

							</div>

						</div>



					</form>

				</div>

			</div>

		</div>

	</div>



</div>



<script>



	$('#duration').timepicker({

		defaultTime: 'value',

		minuteStep: 1,

		disableFocus: true,

		template: 'dropdown',

		showMeridian:false

	});



	$('#session_date_time1').datetimepicker({

		todayHighlight: true,

		autoclose: true,

		format: 'yyyy-mm-dd hh:ii'

	});



	$(document).on('click','.add_more_session',function() {



		session_count = $("#session_count").val();

		session_count = parseInt(session_count) + 1;

		$("#session_count").val(session_count);

		

		var $row = $('.session_clone_div'); 

		var $clone = $('.session_clone_div').clone();

		$clone.find('#session_id').val(0); 

		$clone.find('#session_title').val(''); 

		$clone.find('#session_date_time').val('');

		$clone.find('#session_attendees').val('');

		$clone.find('.session_date_time').attr('id','session_date_time'+session_count);

		$clone.closest('div').attr('class','row');



		$row.after($clone);



		$('#session_date_time'+session_count).datetimepicker({

			todayHighlight: true,

			autoclose: true,

			format: 'yyyy-mm-dd hh:ii'

		});

	});



	$(document).on('click','.remove_session',function(){

		var num_of_session_clone_div = $("#session_count").val();

		if (num_of_session_clone_div != 1) {

			session_count = $("#session_count").val();

			session_count = parseInt(session_count) - 1;

			$("#session_count").val(session_count);



			var obj = $(this).closest('.row');

			obj.remove();

		}

	});



	$(document).on('click','.add_more_chapter',function() {



		chapter_count = $("#chapter_count").val();

		chapter_count = parseInt(chapter_count) + 1;

		$("#chapter_count").val(chapter_count);

		

		var $row = $('.chapter_clone_div'); 

		var $clone = $('.chapter_clone_div').clone();

		$clone.find('#chapter_id').val(0);

		$clone.find('#chapter_title').val(''); 

		$clone.find('#chapter_description').val('');

		$clone.find('#chapter_document').val('');

		$clone.find('#chapter_video').val('');

		$clone.find('#old_chapter_video').val('');

		$clone.find('#old_chapter_document').val('');

		$clone.closest('div').attr('class','row');



		$row.after($clone);

	});



	$(document).on('click','.remove_chapter',function(){

		var num_of_session_clone_div = $("#chapter_count").val();

		if (num_of_session_clone_div != 1) {

			chapter_count = $("#chapter_count").val();

			chapter_count = parseInt(chapter_count) - 1;

			$("#chapter_count").val(chapter_count);



			var obj = $(this).closest('.row');

			obj.remove();

		}

	});



	function typeChange() {

		var type = $("#type").val();

		if(type == 1){

			$('.session_clone').show();

			$('.chapter_clone').hide();

		}else if(type == 2){

			$('.session_clone').hide();

			$('.chapter_clone').show();

		}else{

			$('.session_clone').hide();

			$('.chapter_clone').hide();

		}

	}



	function getSubcate(){

		id = $("#cat_id").val();



		$.ajax({

			type:'POST',

			url:"{{route('get.subCate')}}",

			data:{

				'_token' : $('input[name="_token"]').val(),

				'cat_id':id

			},

			success:function(data){

				$("#subcat_id").html();

				$("#subcat_id").html(data);

			}

		});

	}



	$(".edit__form").validate({

		ignore: ":hidden",

		rules:

		{

			user_id:{required:true},

			title:{required:true},

			description:{required:true},

			cat_id:{required:true},

			subcat_id:{required:true},

			duration:{required:true},

			type:{required:true},

			fees:{required:true},

			"session_title[]":{required:true},

			"session_date_time[]":{required:true},

			"session_attendees[]":{required:true}

		},

		messages:

		{

			user_id:{required:"Please select user"},

			title:{required:"Please enter title"},

			description:{required:"Please enter description"},

			cat_id:{required:"Please select category"},

			subcat_id:{required:"Please select sub category"},

			duration:{required:"Please select duration"},

			type:{required:"Please enter type"},

			fees:{required:"Please enter fees"},

			"session_title[]":{required:"Please enter session title"},

			"session_date_time[]":{required:"Please select session date time"},

			"session_attendees[]":{required:"Please enter no of attendees"}

		}

	});



	$("#save").on("click", function (e)

	{

		e.preventDefault();

		if ($(".edit__form").valid())

		{

			$.ajax({

				type: "POST",

				url: "{{ route('course.update', array($course->id)) }}",

				data: new FormData($('.edit__form')[0]),

				processData: false,

				contentType: false,

				success: function (data)

				{

					if (data.status === 'success') 

					{

						toastr.options.timeOut = 3000;

						toastr.options.fadeOut = 3000;

						toastr.options.progressBar = true;

						toastr.options.onHidden = function(){

							window.location = "{{ route('course.index') }}"

						};



						toastr["success"]("{{$title}} Updated Successfully", "Success");

					}

					else if(data.status === 'error') 

					{

						toastr.options.timeOut = 3000;

						toastr.options.fadeOut = 3000;

						toastr.options.progressBar = true;



						toastr["error"]("Opps.. Something Went Wrong.!", "Error");

					}

				}

			});

		}

		else

		{

			e.preventDefault();

		}

	});

</script>

@stop