@extends('main')
@section('content')

<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content"><br/>
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="kt-font-brand flaticon2-line-chart"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						{{ $title }} List
					</h3>
				</div>
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						
						<div class="kt-portlet__head-actions">
							<a href="{{ route('staticPage.create') }}" class="btn btn-brand btn-elevate btn-icon-sm">
								<i class="la la-plus"></i>
								Add {{$title}}
							</a>
						</div>
						
					</div>
				</div>
			</div>
			<div class="kt-portlet__body">
				<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

					<div class="row">
						<div class="col-sm-12" id="user_list_table">
							<table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="user_list" role="grid" aria-describedby="kt_table_1_info">
								@csrf

								<thead>
									<tr role="row">
										<th>No</th>
										<th>Name</th>
										<th>Activate Status</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>

									@if(!empty($data))
									<?php $count = 1; ?>
									@foreach($data as $dt)
									<tr>
										<td>{{ $count++ }}</td>
										<td>{{ ucfirst($dt->title) }}</td>
										<td>
											<span class="kt-switch kt-switch--sm">
												<label>
													<input type="checkbox" @if($dt->active == 1) checked @endif onchange="status('{{ $dt->id }}')" name="">
													<span></span>
												</label>
											</span>
										</td>
										<td>
											<a href="{{route('staticPage.edit',$dt->id)}}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit" style="background: #968cbd;">
												<i class="la la-edit" style="color: white !important;"></i>
											</a>
											<input type="hidden" value="{{ $dt->id }}" name="delete_user_id" id="delete_user_id">

											<button class="btn btn-sm btn-clean btn-icon btn-icon-md" id="delete_user" style="background-color: red;" data-toggle="tooltip" title="Delete">
												<i class="la la-trash" style="color: #ffffff"></i>
											</button>
										</td>
									</tr>
									@endforeach
									@endif
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function() {
		$('#user_list').DataTable();
	});
</script>
<script>

	function status(id){
		$.ajax({
			type:'POST',
			url:"{{route('staticPage.status')}}",
			data:{
				'_token' : $('input[name="_token"]').val(),
				'id':id
			},
			success:function(data){
				if (data.status === 'status_changed') 
				{
					toastr.options.timeOut = 1500;
					toastr.options.fadeOut = 1500;
					toastr.options.progressBar = true;
					toastr.options.onHidden = function(){
					};

					toastr["success"]("Activation Status Changed", "Success");
				}
			}
		});

	}

	$(document).ready(function() {

		$(document).on('click', '#delete_user', function ()
		{
			var obj = $(this);
			var id = $(this).closest('td').find("#delete_user_id").val();

			swal({
				title: "Are you sure?",
				text: "You will not be able recover this record",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes delete it!",
				closeOnConfirm: false
			},
			function () {
				$.ajax({
					type: "post",
					url: '{{ route('staticPage.delete') }}',
					data: {
						'_token': $('input[name="_token"]').val(),
						'id': id
					},
					cache: false,
					success: function (data)
					{
						obj.closest('tr').remove();
					}
				});
				swal("Deleted", "{{ $title }} has been deleted.", "success");
			});
		});
	});

</script>

@stop