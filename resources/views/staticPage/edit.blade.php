@extends('main')

@section('content')



<script src="{{ asset('assets/plugins/ckeditor/ckeditor.js') }}" type="text/javascript"></script>


<link href="{{ asset('assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />



<link href="{{ asset('sweetalert/dist/sweetalert.css')}}" rel="stylesheet" type="text/css" />





<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content"><br/>





	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

		<div class="kt-portlet kt-portlet--mobile">

			<div class="kt-portlet__head kt-portlet__head--lg">

				<div class="kt-portlet__head-label">

					<span class="kt-portlet__head-icon">

						<i class="kt-font-brand flaticon2-line-chart"></i>

					</span>

					<h3 class="kt-portlet__head-title">

						Add {{ $title }}

					</h3>

				</div>

			</div>

			<div class="kt-portlet__body">

				<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

					<form class="kt-form edit__form" id="edit__form" method="POST">

						@csrf

						@method('PUT')
						
						<div class="row">

							<div class="col-sm-5">

								<label>Tital<code>*</code></label>

								<input type="text" class="form-control" name="title" id="title" placeholder="Enter Tital" autocomplete="off" value="{{ $edit->title }}">

							</div>

							<div class="col-sm-7">

								<label>Link</label>

								<input type="text" class="form-control" name="link" id="link" placeholder="Enter Link" autocomplete="off" value="{{ $edit->link }}">

							</div>

						</div>



						<div class="row">

							<div class="col-sm-12">

								<label>Description<code>*</code></label>

								<textarea class="form-control" id="description" name="description" rows="8">{{ $edit->description }}</textarea>

							</div>

						</div>



						<div class="kt-portlet__foot">

							<div class="kt-form__actions">

								<button type="button" class="btn btn-brand" id="save">Submit</button>

								<a href="{{route('staticPage.index')}}" class="btn btn-dark">Cancel</a>

							</div>

						</div>

					</form>

				</div>

			</div>

		</div>

	</div>



</div>

<script type="text/javascript">
	CKEDITOR.replace( 'description' );
</script>

<script>

	$(".edit__form").validate({

		rules:

		{

			title:{

				required: true,

				remote: {

					url: "{{ route('check.title') }}",

					type: "post",

					dataType: 'json',

					data: {

						'_token': $('input[name="_token"]').val(),

						title: function() {

							return $( "#title" ).val();

						}

					}

				}

			},

			

		},

		messages:

		{

			title:{required:"Please enter title",remote:"This name is already exist"},

			

		}

	});

	$.ajaxSetup({

		headers: {

			'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')

		}

	});

	$("#save").on("click", function (e)

	{

		e.preventDefault();

		if ($(".edit__form").valid())

		{
			for ( instance in CKEDITOR.instances ) {
				CKEDITOR.instances.description.updateElement();
			}
			$.ajax({

				type: "POST",

				url: "{{ route('staticPage.update', array($edit->id)) }}",

				data: new FormData($('.edit__form')[0]),

				processData: false,

				contentType: false,

				success: function (data)

				{

					if (data.status === 'success') 

					{

						toastr.options.timeOut = 3000;

						toastr.options.fadeOut = 3000;

						toastr.options.progressBar = true;

						toastr.options.onHidden = function(){

							window.location = "{{ route('staticPage.index') }}"

						};



						toastr["success"]("{{$title}} Added Successfully", "Success");

					}

					else if(data.status === 'error') 

					{

						toastr.options.timeOut = 3000;

						toastr.options.fadeOut = 3000;

						toastr.options.progressBar = true;



						toastr["error"]("Opps.. Something Went Wrong.!", "Error");

					}

				}

			});

		}

		else

		{

			e.preventDefault();

		}

	});

</script>

@stop