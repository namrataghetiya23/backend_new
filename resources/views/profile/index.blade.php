@extends('main')
@section('content')

<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content"><br/>

	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

		<!--Begin::App-->
		<div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">

			<!--Begin:: App Aside Mobile Toggle-->
			<button class="kt-app__aside-close" id="kt_user_profile_aside_close">
				<i class="la la-close"></i>
			</button>

			<!--End:: App Aside Mobile Toggle-->

			<!--Begin:: App Aside-->
			@include('profile.sideinfo')

			<!--End:: App Aside-->

			<!--Begin:: App Content-->
			<div class="kt-grid__item kt-grid__item--fluid kt-app__content">
				<div class="row">
					<div class="col-xl-12">
						<div class="kt-portlet">
							<div class="kt-portlet__head">
								<div class="kt-portlet__head-label">
									<h3 class="kt-portlet__head-title">Personal Information <small>update your personal informaiton</small></h3>
								</div>
							</div>

							<form class="kt-form kt-form--label-right add__form" id="add__form">
								@csrf
								@method('PUT')
								<div class="kt-portlet__body">
									<div class="kt-section kt-section--first">
										<div class="kt-section__body">
											<div class="form-group row">
												<label class="col-xl-3 col-lg-3 col-form-label">First Name <code>*</code></label>
												<div class="col-lg-9 col-xl-6">
													<input class="form-control" type="text" name="name" id="name" value="{{ $data->name }}">
												</div>
											</div>
											<div class="form-group row">
												<label class="col-xl-3 col-lg-3 col-form-label">Last Name <code>*</code></label>
												<div class="col-lg-9 col-xl-6">
													<input class="form-control" type="text" name="lname" id="lname" value="{{ $data->lname }}">
												</div>
											</div>
											<div class="form-group row">
												<label class="col-xl-3 col-lg-3 col-form-label">Contact Phone <code>*</code></label>
												<div class="col-lg-9 col-xl-6">
													<div class="input-group">
														<div class="input-group-prepend">
															<span class="input-group-text">
																<i class="la la-phone"></i>
															</span>
														</div>
														<input type="text" class="form-control" value="{{ $data->contact }}" id="contact" name="contact" placeholder="Enter Contact Phone" aria-describedby="basic-addon1">
													</div>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-xl-3 col-lg-3 col-form-label">Email Address <code>*</code></label>
												<div class="col-lg-9 col-xl-6">
													<div class="input-group">
														<div class="input-group-prepend">
															<span class="input-group-text">
																<i class="la la-at"></i>
															</span>
														</div>
														<input type="text" class="form-control" value="{{ $data->email }}" id="email" name="email" placeholder="Enter Email" aria-describedby="basic-addon1">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__foot">
									<div class="kt-form__actions">
										<div class="row">
											<div class="col-lg-3 col-xl-3">
											</div>
											<div class="col-lg-9 col-xl-9">
												<button type="button" id="save" class="btn btn-brand">Submit</button>&nbsp;
												<button type="reset" class="btn btn-dark">Reset</button>
											</div>
										</div>
									</div>
								</div>
							</form>

						</div>
					</div>
				</div>
			</div>

			<!--End:: App Content-->
		</div>

		<!--End::App-->
	</div>
</div>

<script>
	$(".add__form").validate({
		rules:
		{
			name:{required:true},
			lname:{required:true},
			email:{
				required: true,
				email: true,
				remote: {
					url: "{{ route('check.adminEmail') }}",
					type: "post",
					dataType: 'json',
					data: {
						'_token': $('input[name="_token"]').val(),
						id: function() {
							return <?php echo Auth::user()->id ?>;
						},
						email: function() {
							return $( "#email" ).val();
						}
					}
				}
			},
			contact:{required:true}
		},
		messages:
		{
			name:{required:"Please enter first name"},
			lname:{required:"Please enter last name"},
			email:{
				required:"Please enter email id",
				email:"Please enter valid formate",
				remote:"This email is already exist"
			},
			contact:{required:"Please enter contact phone"}
		}
	});

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
		}
	});

	$("#save").on("click", function (e)
	{
		e.preventDefault();
		if ($(".add__form").valid())
		{
			$.ajax({
				type: "POST",
				url: "{{ route('profile.update', array($data->id)) }}",
				data: new FormData($('.add__form')[0]),
				processData: false,
				contentType: false,
				success: function (data)
				{
					if (data.status === 'success') 
					{
						toastr.options.timeOut = 3000;
						toastr.options.fadeOut = 3000;
						toastr.options.progressBar = true;
						toastr.options.onHidden = function(){
							window.location = "{{ route('profile.index') }}"
						};

						toastr["success"]("profile Updated Successfully", "Success");
					}
					else if(data.status === 'error') 
					{
						toastr.options.timeOut = 3000;
						toastr.options.fadeOut = 3000;
						toastr.options.progressBar = true;

						toastr["error"]("Opps.. Something Went Wrong.!", "Error");
					}
				}
			});
		}
		else
		{
			e.preventDefault();
		}
	});
</script>

@stop