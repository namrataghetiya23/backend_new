@extends('main')
@section('content')

<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content"><br/>

	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

		<!--Begin::App-->
		<div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">

			<!--Begin:: App Aside Mobile Toggle-->
			<button class="kt-app__aside-close" id="kt_user_profile_aside_close">
				<i class="la la-close"></i>
			</button>

			<!--End:: App Aside Mobile Toggle-->

			<!--Begin:: App Aside-->
			@include('profile.sideinfo')
			<!--End:: App Aside-->

			<!--Begin:: App Content-->
			<div class="kt-grid__item kt-grid__item--fluid kt-app__content">
				<div class="row">
					<div class="col-xl-12">
						<div class="kt-portlet kt-portlet--height-fluid">
							<div class="kt-portlet__head">
								<div class="kt-portlet__head-label">
									<h3 class="kt-portlet__head-title">Change Password<small>change or reset your account password</small></h3>
								</div>
							</div>
							<form class="kt-form kt-form--label-right add__form" id="add__form">
								@csrf
								@method('PUT')
								<div class="kt-portlet__body">
									<div class="kt-section kt-section--first">
										<div class="kt-section__body">
											<div class="form-group row">
												<label class="col-xl-3 col-lg-3 col-form-label">Current Password</label>
												<div class="col-lg-9 col-xl-6">
													<input type="password" class="form-control" value="" placeholder="Current password" id="cPassword" name="cPassword">
												</div>
											</div>
											<div class="form-group row">
												<label class="col-xl-3 col-lg-3 col-form-label">New Password</label>
												<div class="col-lg-9 col-xl-6">
													<input type="password" class="form-control" value="" placeholder="New password" id="password" name="password">
												</div>
											</div>
											<div class="form-group form-group-last row">
												<label class="col-xl-3 col-lg-3 col-form-label">Verify Password</label>
												<div class="col-lg-9 col-xl-6">
													<input type="password" class="form-control" value="" placeholder="Verify password" id="vPassword" name="vPassword">
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__foot">
									<div class="kt-form__actions">
										<div class="row">
											<div class="col-lg-3 col-xl-3">
											</div>
											<div class="col-lg-9 col-xl-9">
												<button type="button" id="save" class="btn btn-brand btn-bold">Change Password</button>&nbsp;
												<button type="reset" class="btn btn-dark">Cancel</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>

			<!--End:: App Content-->
		</div>

		<!--End::App-->
	</div>
</div>

<script>
	$(".add__form").validate({
		rules:
		{
			cPassword:{
				required: true,
				remote: {
					url: "{{ route('check.password') }}",
					type: "post",
					dataType: 'json',
					data: {
						'_token': $('input[name="_token"]').val(),
						id: function() {
							return <?php echo Auth::user()->id ?>;
						},
						cPassword: function() {
							return $( "#cPassword" ).val();
						}
					}
				}
			},
			password:{required:true},
			vPassword:{required:true,equalTo : "#password"}
		},
		messages:
		{
			cPassword:{
				required:"Please enter current password",
				remote:"This is Wrong current password"
			},
			password:{required:"Please enter password"},
			vPassword:{required:"Please enter verify password",equalTo:"Please enter sane as new password"}
		}
	});

	$("#save").on("click", function (e)
	{
		e.preventDefault();
		if ($(".add__form").valid())
		{
			$.ajax({
				type: "POST",
				url: "{{ route('profile.update', array($data->id)) }}",
				data: new FormData($('.add__form')[0]),
				processData: false,
				contentType: false,
				success: function (data)
				{
					if (data.status === 'success') 
					{
						toastr.options.timeOut = 3000;
						toastr.options.fadeOut = 3000;
						toastr.options.progressBar = true;
						toastr.options.onHidden = function(){
							window.location = "{{ route('profile.index') }}"
						};

						toastr["success"]("profile Updated Successfully", "Success");
					}
					else if(data.status === 'error') 
					{
						toastr.options.timeOut = 3000;
						toastr.options.fadeOut = 3000;
						toastr.options.progressBar = true;

						toastr["error"]("Opps.. Something Went Wrong.!", "Error");
					}
				}
			});
		}
		else
		{
			e.preventDefault();
		}
	});
</script>

@stop