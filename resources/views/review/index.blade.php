@extends('main')

@section('content')



<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content"><br/>



	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

		<div class="kt-portlet kt-portlet--mobile">

			<div class="kt-portlet__head kt-portlet__head--lg">

				<div class="kt-portlet__head-label">

					<span class="kt-portlet__head-icon">

						<i class="kt-font-brand flaticon2-line-chart"></i>

					</span>

					<h3 class="kt-portlet__head-title">

						{{ $title }} List

					</h3>

				</div>

			</div>

			<div class="kt-portlet__body">

				<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">



					<div class="row">

						<div class="col-sm-12" id="review_list_table">

							<table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="review_list" role="grid" aria-describedby="kt_table_1_info">

								@csrf



								<thead>

									<tr role="row">

										<th>No</th>

										<th>Who Give</th>
										<th>To Whom</th>
										<th>Review For</th>
										<th>Description</th>
										<th>Rating</th>
										<th>Date</th>
										<th>Activate Status</th>
										<th>Actions</th>

									</tr>

								</thead>

								<tbody>



									@if(!empty($data))

									<?php $count = 1; ?>

									@foreach($data as $dt)

									<tr>

										<td>{{ $count++ }}</td>

										<td>{{ ucfirst($dt->name) }}{{ ucfirst($dt->lname) }}</td>
										<td>Alma Albert Beverly Justice</td>

										@php $type = '' @endphp
										@if($dt->type == 1)
										@php $type = 'Subject' @endphp
										@else
										@php $type = 'Course' @endphp
										@endif
										<td>{{ $type }}</td>
										<td>{{ $dt->description }}</td>
										<td>{{ $dt->rating }}</td>
										<td>15 Jan,2020</td>
										<td>

											<span class="kt-switch kt-switch--sm">

												<label>

													<input type="checkbox" @if($dt->active == 1) checked @endif onchange="status('{{ $dt->id }}')" name="">

													<span></span>

												</label>

											</span>

										</td>

										<td>



											<a data-toggle="modal" data-target="#edit_review" onclick="edit('{{ $dt->id }}')" class="btn btn-sm btn-clean btn-icon btn-icon-md edit-icon" title="Edit" style="background: #968cbd;">

												<i class="la la-edit" style="color: #ffffff"></i>

											</a>

											<input type="hidden" value="{{ $dt->id }}" name="delete_review_id" id="delete_review_id">



											<button class="btn btn-sm btn-clean btn-icon btn-icon-md" id="delete_review" style="background-color: red;" data-toggle="tooltip" title="Delete">

												<i class="la la-trash" style="color: #ffffff"></i>

											</button>

										</td>

									</tr>

									@endforeach

									@endif

								</tbody>

							</table>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>


<!--Edit Modal-->

<div class="modal fade" id="edit_review" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">

	<div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<h5 class="modal-title" id="exampleModalLabel">Edit {{ $title }}</h5>

				<button type="button" class="close" data-dismiss="modal" aria-label="Close">

					<i aria-hidden="true" class="ki ki-close"></i>

				</button>

			</div>

			<div class="modal-body">

				<form class="edit__form" method="post">
					@csrf
					<input type="hidden" name="review_hidden_id" value="" id="hidden_id">
					<div data-scroll="true" data-height="auto">

						<div class="form-group row">

							<label class="col-xl-3 col-lg-3 col-form-label">User</label>

							<div class="col-lg-9 col-xl-9">

								<select class="form-control" name="user_id" id="edit_user_id" required="required">

									<option value="">--Select User--</option>
									@if(!empty($user))
									@foreach($user as $user_data)
									<option value="{{ $user_data->id }}">{{ $user_data->name }}{{ $user_data->lname }}</option>
									@endforeach
									@endif
								</select>

							</div>

						</div>

						<div class="form-group row">

							<label class="col-xl-3 col-lg-3 col-form-label">Review For</label>

							<div class="col-lg-9 col-xl-9">

								<select class="form-control" id="type" name="type">
									<option value="">--Select Type--</option>
									<option value="1">Subject</option>
									<option value="2">Course</option>
								</select>

							</div>

						</div>
						<div class="form-group row">

							<label class="col-xl-3 col-lg-3 col-form-label">Description</label>

							<div class="col-lg-9 col-xl-9">

								<textarea class="form-control" id="description" name="description"></textarea>

							</div>

						</div>
						<div class="form-group row">

							<label class="col-xl-3 col-lg-3 col-form-label">Rating</label>

							<div class="col-lg-9 col-xl-9">

								<select class="form-control" id="rating" name="rating">
									<option value="">--Select Rating--</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
								</select>

							</div>

						</div>
						<div class="modal-footer">

							<button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>

							<button type="button" id="save" class="btn btn-brand btn-elevate btn-icon-sm font-weight-bold">Update</button>

						</div>

					</div>

				</div>

			</form>

		</div>

	</div>

</div>

<script>

	$(document).ready(function() {

		$('#review_list').DataTable();

	});

</script>

<script>

	$(".edit__form").validate({

		rules:

		{
			user_id:{required:true},
			type:{required:true},
			rating:{required:true}
		},

		messages:

		{
			user_id:{required:"Please select user"},
			type:{required:"Please select type"},
			rating:{required:"Please select rating"}

		}

	});


	function status(id){

		$.ajax({

			type:'POST',

			url:"{{route('review.status')}}",

			data:{

				'_token' : $('input[name="_token"]').val(),

				'id':id

			},

			success:function(data){

				if (data.status === 'status_changed') 

				{

					toastr.options.timeOut = 1500;

					toastr.options.fadeOut = 1500;

					toastr.options.progressBar = true;

					toastr.options.onHidden = function(){

						// location.reload();

					};



					toastr["success"]("Activation Status Changed", "Success");

				}

			}

		});

		

	}	



	function edit(id){



		$.ajax({

			type: "post",

			url: "{{ route('review.edits') }}",

			data: {

				'_token': $('input[name="_token"]').val(),

				'id': id

			},

			cache: false,

			success: function (data)

			{

				$('#hidden_id').val(data.review_data['id']);

				$('#edit_user_id').val(data.review_data['user_id']);

				$('#type').val(data.review_data['type']);
				$('#rating').val(data.review_data['rating']);
				$('#description').val(data.review_data['description']);

			}

		});

	}



	$(document).ready(function() {



		$(document).on('click', '#delete_review', function ()

		{

			var obj = $(this);

			var id = $(this).closest('td').find("#delete_review_id").val();



			swal({

				title: "Are you sure?",

				text: "You will not be able recover this record",

				type: "warning",

				showCancelButton: true,

				confirmButtonColor: "#DD6B55",

				confirmButtonText: "Yes delete it!",

				closeOnConfirm: false

			},

			function () {

				$.ajax({

					type: "post",

					url: '{{ route('delete.review') }}',

					data: {

						'_token': $('input[name="_token"]').val(),

						'id': id

					},

					cache: false,

					success: function (data)

					{

						obj.closest('tr').remove();

					}

				});

				swal("Deleted", "{{ $title }} has been deleted.", "success");

			});

		});

	});

	$("#save").on("click", function (e)

	{

		e.preventDefault();

		if ($(".edit__form").valid())

		{

			$.ajax({

				type: "POST",

				url: "{{ route('update.review') }}",

				data: new FormData($('.edit__form')[0]),

				processData: false,

				contentType: false,

				success: function (data)

				{

					if (data.status === 'success') 

					{

						toastr.options.timeOut = 3000;

						toastr.options.fadeOut = 3000;

						toastr.options.progressBar = true;

						toastr.options.onHidden = function(){

							window.location = "{{ route('review.index') }}"

						};



						toastr["success"]("Review Rating Updated Successfully", "Success");

					}

					else if(data.status === 'error') 

					{

						toastr.options.timeOut = 3000;

						toastr.options.fadeOut = 3000;

						toastr.options.progressBar = true;

						toastr.options.onHidden = function(){

							window.location = "{{ route('review.index') }}"

						};



						toastr["error"]("Opps.. Something Went Wrong.!", "Error");

					}

				}

			});

		}

		else

		{

			e.preventDefault();

		}

	});
	

</script>



@stop