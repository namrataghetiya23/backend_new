<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;
use Str;
use DB;

class RoleController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function __construct(Role $s)
	{
		$this->middleware('auth');
	}

	public function index()
	{
		$data['title'] = 'User Roles';
		$data['data'] = Role::get();
		return view('role.index')->with($data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$data['title'] = "User Role";
		return view('role.create')->with($data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$param = $request->all();
		unset($param['user_id']);
		$param['slug'] = Str::slug($param['title']);
		$create = Role::create($param);
		if ($create) 
		{
			return redirect()->back();
		} 
		else 
		{
			return redirect()->back();
		}
	}


	public function status(Request $request)
	{
		$role = Role::where('id',$request->get('id'))->value('active');
		if($role == 1)
		{
			$update = Role::where('id',$request->get('id'))->update(['active' => 0]);
		}
		if($role == 0)
		{
			$update = Role::where('id',$request->get('id'))->update(['active' => 1]);
		}
		if($update)
		{
			return response()->json(['status' => 'status_changed']);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Request $request)
	{
		$data['title'] = "User Role";
		$data['role_data'] = Role::where('id',$request->get('id'))->first();
		return response()->json($data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request)
	{
		// $role = Role::findor
		$param['title'] = $request->title;
		$update = Role::where('id',$request->role_hidden_id)->update($param);
		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function delete(Request $request)
	{
		
	}

	public function checkRole(Request $request)
	{
		$param = $request->all();
		if($param['title'] != ""){
			$param['title'] = Str::slug($param['title']);
			if($param['type'] == 'Add'){
				$check = Role::where('slug',$param['title'])->exists();
			}else{
				$check = Role::where('slug',$param['title'])->where('id','!=',$param['id'])->exists();
			}

			if($check){
				echo json_encode(false);
			}else{
				echo json_encode(true);
			}
		}
	}
}
