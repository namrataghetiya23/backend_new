<?php



namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;

use App\Models\Testimonials;

use Str;

use DB;



class TestimonialsController extends Controller

{

	/**

	 * Display a listing of the resource.

	 *

	 * @return \Illuminate\Http\Response

	 */

	

	public function __construct(FrontUser $s)

	{

		$this->middleware('auth');

	}



	public function index(Request $request)

	{

		$data['title'] = 'User';

		$data['data'] = Testimonials::where('active',1)->get();

		return view('testimonials.index')->with($data);
	}



	/**

	 * Show the form for creating a new resource.

	 *

	 * @return \Illuminate\Http\Response

	 */

	public function create()

	{

		$data['title'] = "Testimonials";

		return view('testimonials.create')->with($data);

	}



	/**

	 * Store a newly created resource in storage.

	 *

	 * @param  \Illuminate\Http\Request  $request

	 * @return \Illuminate\Http\Response

	 */

	public function store(Request $request)

	{

		$param = $request->all();

		$create = Testimonials::create($param);

		if($create)

		{

			return response()->json(['status' => 'success']);

		}

		else

		{

			return response()->json(['status' => 'error']);

		}

	}





	public function status(Request $request)

	{

		$user = Testimonials::where('id',$request->get('id'))->value('active');

		if($user == 1)

		{

			$update = Testimonials::where('id',$request->get('id'))->update(['active' => 0]);

		}

		if($user == 0)

		{

			$update = Testimonials::where('id',$request->get('id'))->update(['active' => 1]);

		}

		if($update)

		{

			return response()->json(['status' => 'status_changed']);

		}

	}

	public function show($id)
	{

	}

	public function edit($id)

	{

		$data['title'] = 'Testimonials';

		$data['edit'] = Testimonials::FindOrFail($id);

		return view('testimonials.edit')->with($data);

	}


	public function update(Request $request, $id)

	{

		$param = $request->all();

		$update = Testimonials::where('id',$id)->update($param);

		if($update)

		{

			return response()->json(['status' => 'success']);

		}

		else

		{

			return response()->json(['status' => 'error']);

		}

	}



	/**

	 * Remove the specified resource from storage.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function delete(Request $request)

	{

		$delete = Testimonials::where('id',$request->get('id'))->delete();

		if ($delete) 

		{

			return response()->json(['status' => 'success']);

		}

	}

}

