<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Str;
use DB;

class ProfileController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function __construct(User $s)
	{
		$this->middleware('auth');	
	}

	public function index(Request $request)
	{
		$data['title'] = 'Profile Information';
		$data['data'] = User::FindOrFail(Auth::user()->id);
		$data['class1'] = "kt-widget__item--active";
		$data['class2'] = "";
		return view('profile.index')->with($data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function changePassword(Request $request)
	{
		$data['title'] = 'Change Password';
		$data['data'] = User::FindOrFail(Auth::user()->id);
		$data['class2'] = "kt-widget__item--active";
		$data['class1'] = "";
		return view('profile.changePassword')->with($data);
	}

	public function update(Request $request, $id)
	{
		$param = $request->all();
		unset($param['_token'],$param['_method']);
		$update = User::where('id',$id)->update($param);
		if($update)
		{
			return response()->json(['status' => 'success']);
		}
		else
		{
			return response()->json(['status' => 'error']);
		}
	}

	public function checkPassword(Request $request)
	{
		$param = $request->all();
		if($param['cPassword'] != ""){

			$cPassword = Hash::make($param['cPassword']);
			if($param['id'] > 0){
				$check = User::where('password',$cPassword)->where('id',$param['id'])->exists();
			}

			if($check){
				echo json_encode(true);
			}else{
				echo json_encode(false);
			}
		}
	}

}
