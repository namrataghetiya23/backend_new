<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\User;

use Str;

use DB;



class AdminController extends Controller

{

	/**

	 * Display a listing of the resource.

	 *

	 * @return \Illuminate\Http\Response

	 */

	public function __construct(User $s)

	{

		$this->middleware('auth');

	}



	public function index(Request $request)

	{

		$data['title'] = 'Admin';

		$data['data'] = User::get();

		return view('admin.index')->with($data);

	}



	/**

	 * Show the form for creating a new resource.

	 *

	 * @return \Illuminate\Http\Response

	 */

	public function create()

	{

		$data['title'] = "Admin";

		return view('admin.create')->with($data);

	}



	/**

	 * Store a newly created resource in storage.

	 *

	 * @param  \Illuminate\Http\Request  $request

	 * @return \Illuminate\Http\Response

	 */

	public function store(Request $request)

	{

		$param = $request->all();

		unset($param['_token']);

		$param['password'] = Hash::make($param['password']);
		
		$create = User::create($param);

		if($create)

		{

			return response()->json(['status' => 'success']);

		}

		else

		{

			return response()->json(['status' => 'error']);

		}

	}





	public function status(Request $request)

	{

		$user = User::where('id',$request->get('id'))->value('active');

		if($user == 1)

		{

			$update = User::where('id',$request->get('id'))->update(['active' => 0]);

		}

		if($user == 0)

		{

			$update = User::where('id',$request->get('id'))->update(['active' => 1]);

		}

		if($update)

		{

			return response()->json(['status' => 'status_changed']);

		}

	}



	/**

	 * Display the specified resource.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function show($id)

	{

		//

	}



	/**

	 * Show the form for editing the specified resource.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function edit($id)

	{

		$data['title'] = 'User';

		$data['edit'] = User::FindOrFail($id);

		return view('admin.edit')->with($data);

	}



	/**

	 * Update the specified resource in storage.

	 *

	 * @param  \Illuminate\Http\Request  $request

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function update(Request $request, $id)

	{

		$param = $request->all();

		unset($param['_token'],$param['_method']);

		$update = User::where('id',$id)->update($param);

		if($update)

		{

			return response()->json(['status' => 'success']);

		}

		else

		{

			return response()->json(['status' => 'error']);

		}

	}



	/**

	 * Remove the specified resource from storage.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function delete(Request $request)

	{

		$delete = User::where('id',$request->get('id'))->delete();

		if ($delete) 

		{

			return response()->json(['status' => 'success']);

		}

	}



	public function checkEmail(Request $request)

	{

		$param = $request->all();

		if($param['email'] != ""){

			if(isset($param['id'])){

				if($param['id'] > 0){

					$check = User::where('email',$param['email'])->where('id','!=',$param['id'])->exists();

				}

			}else{

				$check = User::where('email',$param['email'])->exists();

			}

			if($check){

				echo json_encode(false);

			}else{

				echo json_encode(true);

			}

		}

	}

}

