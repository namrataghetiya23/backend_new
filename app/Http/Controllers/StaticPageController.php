<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\Models\Role;

use App\Models\StaticPage;

use Str;

use DB;



class StaticPageController extends Controller

{

	/**

	 * Display a listing of the resource.

	 *

	 * @return \Illuminate\Http\Response

	 */

	public function __construct(StaticPage $s)

	{

		$this->middleware('auth');

	}



	public function index(Request $request)

	{

		$data['title'] = 'Static Page';

		$data['data'] = StaticPage::get();

		return view('staticPage.index')->with($data);

	}



	/**

	 * Show the form for creating a new resource.

	 *

	 * @return \Illuminate\Http\Response

	 */

	public function create()

	{

		$data['title'] = 'Static Page';

		return view('staticPage.create')->with($data);

	}



	/**

	 * Store a newly created resource in storage.

	 *

	 * @param  \Illuminate\Http\Request  $request

	 * @return \Illuminate\Http\Response

	 */

	public function store(Request $request)

	{

		$param = $request->all();

		unset($param['_token']);

		$create = StaticPage::create($param);

		if($create)

		{

			return response()->json(['status' => 'success']);

		}

		else

		{

			return response()->json(['status' => 'error']);

		}

	}





	public function status(Request $request)

	{

		$user = StaticPage::where('id',$request->get('id'))->value('active');

		if($user == 1)

		{

			$update = StaticPage::where('id',$request->get('id'))->update(['active' => 0]);

		}

		if($user == 0)

		{

			$update = StaticPage::where('id',$request->get('id'))->update(['active' => 1]);

		}

		if($update)

		{

			return response()->json(['status' => 'status_changed']);

		}

	}



	/**

	 * Display the specified resource.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function show($id)

	{

		//

	}



	/**

	 * Show the form for editing the specified resource.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function edit($id)

	{

		$data['title'] = 'User';

		$data['edit'] = StaticPage::FindOrFail($id);

		return view('staticPage.edit')->with($data);

	}



	/**

	 * Update the specified resource in storage.

	 *

	 * @param  \Illuminate\Http\Request  $request

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function update(Request $request, $id)

	{

		$param = $request->all();

		unset($param['_token'],$param['_method']);

		$update = StaticPage::where('id',$id)->update($param);

		if($update)

		{

			return response()->json(['status' => 'success']);

		}

		else

		{

			return response()->json(['status' => 'error']);

		}

	}



	/**

	 * Remove the specified resource from storage.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function delete(Request $request)

	{

		$delete = StaticPage::where('id',$request->get('id'))->delete();

		if ($delete) 

		{

			return response()->json(['status' => 'success']);

		}

	}



	public function checkTitle(Request $request)

	{

		$param = $request->all();

		if($param['title'] != ""){

			$param['slug'] = Str::slug($param['title']);

			if(isset($param['id'])){

				if($param['id'] > 0){

					$check = StaticPage::where('slug',$param['slug'])->where('id','!=',$param['id'])->exists();

				}

			}else{

				$check = StaticPage::where('slug',$param['slug'])->exists();

			}



			if($check){

				echo json_encode(false);

			}else{

				echo json_encode(true);

			}

		}

	}

}

