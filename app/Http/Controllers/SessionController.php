<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\Models\Session;

use App\Models\FrontUser;

use App\Models\SubCategory;

use App\Models\Category;

use App\Models\SessionGroup;

use App\Models\SessionOneToOne;

use Str;

use DB;



class SessionController extends Controller

{

	/**

	 * Display a listing of the resource.

	 *

	 * @return \Illuminate\Http\Response

	 */

	public function __construct(Session $s)

	{

		$this->middleware('auth');

	}



	public function index(Request $request)

	{

		$data['title'] = 'Session';

		$data['data'] = Session::leftjoin('users as us','us.id','session.user_id')->select('session.*','us.name as name','us.lname as lname')->get();

		return view('session.index')->with($data);

	}



	/**

	 * Show the form for creating a new resource.

	 *

	 * @return \Illuminate\Http\Response

	 */

	public function create()

	{

		$data['title'] = "Session";

		$data['user'] = FrontUser::where('active',1)->get();

		$data['category'] = Category::where('active',1)->where('type',1)->get();

		return view('session.create')->with($data);

	}



	/**

	 * Store a newly created resource in storage.

	 *

	 * @param  \Illuminate\Http\Request  $request

	 * @return \Illuminate\Http\Response

	 */

	public function store(Request $request)

	{

		// dd($request->all());

		if($request->hasFile('sub_image'))

		{

			$sub_image_name = time().'-'.$request->sub_image->getClientOriginalName();

			request()->sub_image->move(public_path('/session_img/'), $sub_image_name);

		}



		$add_session = new Session();

		$add_session->user_id = $request->user_id;

		$add_session->sub_title = $request->sub_title;

		$add_session->sub_desc = $request->sub_desc;

		$add_session->sub_category = $request->sub_category;

		$add_session->sub_subCategory = $request->sub_subCategory;

		$add_session->sub_duration = $request->sub_duration;

		$add_session->sub_type = $request->sub_type;

		$add_session->sub_image = $sub_image_name;

		$add_session->save();



		if($request->sub_type == 1){

			

			$add_oto_session = new SessionOneToOne();

			$add_oto_session->session_id = $add_session->id;

			$add_oto_session->session_title = $request->session_title;

			$add_oto_session->session_hr_price = $request->session_hr_price;

			$add_oto_session->session_grade = $request->session_grade;

			$add_oto_session->save();	



			if($add_oto_session)

			{

				return response()->json(['status' => 'success']);

			}

			else

			{

				return response()->json(['status' => 'error']);

			}

		}

		else{

			for ($i = 0; $i < count($request['session_grp_title']); $i++) {



				$add_session_grup = new SessionGroup();

				$add_session_grup->session_id = $add_session->id;

				$add_session_grup->session_grp_title = $request['session_grp_title'][$i];

				$add_session_grup->session_grup_date_time = $request['session_grup_date_time'][$i];

				$add_session_grup->session_grup_fess = $request['session_grup_fess'][$i];

				$add_session_grup->session_grup_attendees = $request['session_grup_attendees'][$i];

				$add_session_grup->session_grup_grade = $request['session_grup_grade'][$i];

				$add_session_grup->save();

			}



			if($add_session_grup)

			{

				return response()->json(['status' => 'success']);

			}

			else

			{

				return response()->json(['status' => 'error']);

			}

		}

	}



	public function status(Request $request)

	{

		$user = Session::where('id',$request->get('id'))->value('active');

		if($user == 1)

		{

			$update = Session::where('id',$request->get('id'))->update(['active' => 0]);

		}

		if($user == 0)

		{

			$update = Session::where('id',$request->get('id'))->update(['active' => 1]);

		}

		if($update)

		{

			return response()->json(['status' => 'status_changed']);

		}

	}



	/**

	 * Display the specified resource.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function show($id)

	{

		//

	}



	/**

	 * Show the form for editing the specified resource.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function edit($id)

	{

		$data['title'] = "Session";

		$data['user'] = FrontUser::where('active',1)->get();

		$data['category'] = Category::where('active',1)->where('type',1)->get();



		$data['session'] = Session::FindOrFail($id);

		$data['subCategory'] = SubCategory::where('cat_id',$data['session']['sub_category'])->where('active',1)->get();



		$data['SessionGroup'] = SessionGroup::where('session_id',$id)->get();

		$data['SessionOneToOne'] = SessionOneToOne::where('session_id',$id)->first();

		

		return view('session.edit')->with($data);

	}



	/**

	 * Update the specified resource in storage.

	 *

	 * @param  \Illuminate\Http\Request  $request

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function update(Request $request, $id)

	{
		// dd($request->all());

		if($request->hasFile('sub_image'))

		{

			$sub_image_name = time().'-'.$request->sub_image->getClientOriginalName();

			request()->sub_image->move(public_path('/sub_image/'), $sub_image_name);

			unlink(public_path('/sub_image/'.$request->hide_sub_image));

		}else{

			$sub_image_name = $request->hide_sub_image;

		}



		$add_session = Session::Findorfail($id);

		$add_session->user_id = $request->user_id;

		$add_session->sub_title = $request->sub_title;

		$add_session->sub_desc = $request->sub_desc;

		$add_session->sub_category = $request->sub_category;

		$add_session->sub_subCategory = $request->sub_subCategory;

		$add_session->sub_duration = $request->sub_duration;

		$add_session->sub_type = $request->sub_type;

		$add_session->sub_image = $sub_image_name;

		$add_session->save();



		if($request->sub_type == 1){

			$delete = SessionGroup::where('session_id',$add_session->id)->delete();

			if($request->session_oto_id > 0){

				$add_oto_session = SessionOneToOne::Findorfail($request->session_oto_id);

				$add_oto_session->session_id = $add_session->id;

				$add_oto_session->session_title = $request->session_title;

				$add_oto_session->session_hr_price = $request->session_hr_price;

				$add_oto_session->session_grade = $request->session_grade;

				$add_oto_session->save();	

			}else{

				$new_oto_session = new SessionOneToOne();

				$new_oto_session->session_id = $add_session->id;

				$new_oto_session->session_title = $request->session_title;

				$new_oto_session->session_hr_price = $request->session_hr_price;

				$new_oto_session->session_grade = $request->session_grade;

				$new_oto_session->save();

			}


			return response()->json(['status' => 'success']);
			
		}

		else if($request->sub_type == 2){

			$delete = SessionOneToOne::where('session_id',$add_session->id)->delete();

			for ($i = 0; $i < count($request['session_grp_title']); $i++) {
				if($request['session_grup_id'][$i] > 0){
					$add_session_grup = SessionGroup::Findorfail($request['session_grup_id'][$i]);

					$add_session_grup->session_id = $add_session->id;

					$add_session_grup->session_grp_title = $request['session_grp_title'][$i];

					$add_session_grup->session_grup_date_time = $request['session_grup_date_time'][$i];

					$add_session_grup->session_grup_fess = $request['session_grup_fess'][$i];

					$add_session_grup->session_grup_attendees = $request['session_grup_attendees'][$i];

					$add_session_grup->session_grup_grade = $request['session_grup_grade'][$i];

					$add_session_grup->save();
				}else{
					$new_session_grup = new SessionGroup();

					$new_session_grup->session_id = $add_session->id;

					$new_session_grup->session_grp_title = $request['session_grp_title'][$i];

					$new_session_grup->session_grup_date_time = $request['session_grup_date_time'][$i];

					$new_session_grup->session_grup_fess = $request['session_grup_fess'][$i];

					$new_session_grup->session_grup_attendees = $request['session_grup_attendees'][$i];

					$new_session_grup->session_grup_grade = $request['session_grup_grade'][$i];

					$new_session_grup->save();
				}
			}

			return response()->json(['status' => 'success']);

		}

		else{

			return response()->json(['status' => 'error']);
		}

	}



	/**

	 * Remove the specified resource from storage.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function delete(Request $request)

	{

		$delete = Session::where('id',$request->get('id'))->delete();
		$delete_session_grup = SessionGroup::where('session_id',$delete->id)->delete();
		$delete_session_oto = SessionOneToOne::where('session_id',$delete->id)->delete();
		
		if ($delete) 

		{

			return response()->json(['status' => 'success']);

		}

	}



	public function getSubcate(Request $request)

	{

		$param = $request->all();

		

		$sub_cat = SubCategory::where('cat_id',$param['cat_id'])->where('active',1)->get();

		$html = "<option value=''>--Select Sub Category--</option>";

		if(!empty($sub_cat)){

			foreach ($sub_cat as $sub_cat_data) {

				$html .= "<option value='".$sub_cat_data->id."'>".$sub_cat_data->sub_name."</option>";

			}

		}

		echo $html;

	}

}

