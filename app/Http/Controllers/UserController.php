<?php



namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;

use App\Models\Role;

use App\Models\FrontUser;

use Str;

use DB;



class UserController extends Controller

{

	/**

	 * Display a listing of the resource.

	 *

	 * @return \Illuminate\Http\Response

	 */

	

	public function __construct(FrontUser $s)

	{

		$this->middleware('auth');

	}



	public function index(Request $request)

	{

		$data['title'] = 'User';

		

		if($request->id != null){

			$data['data'] = FrontUser::leftJoin('roles as rl','rl.id','front_user.role_id')

			->select('rl.title','front_user.*')

			->where('role_id',$request->id)

			->get();

		}

		else{

			$data['data'] = FrontUser::leftJoin('roles as rl','rl.id','front_user.role_id')

			->select('rl.title','front_user.*')->get();

		}



		$data['role'] = Role::where('active',1)->get();



		return view('user.index')->with($data);

	}



	/**

	 * Show the form for creating a new resource.

	 *

	 * @return \Illuminate\Http\Response

	 */

	public function create()

	{

		$data['title'] = "User";

		$data['role'] = Role::where('active',1)->get();

		return view('user.create')->with($data);

	}



	/**

	 * Store a newly created resource in storage.

	 *

	 * @param  \Illuminate\Http\Request  $request

	 * @return \Illuminate\Http\Response

	 */

	public function store(Request $request)

	{

		$param = $request->all();
		// dd($param);
		unset($param['cPassword'],$param['_token']);

		if($request->hasFile('document'))

		{

			$document_name = time().'-'.$request->document->getClientOriginalName();

			request()->document->move(public_path('/user_doc/'), $document_name);

			$param['document'] = $document_name;
		}	

		$param['password'] = Hash::make($param['password']);

		
		$create = FrontUser::create($param);

		if($create)

		{

			return response()->json(['status' => 'success']);

		}

		else

		{

			return response()->json(['status' => 'error']);

		}

	}





	public function status(Request $request)

	{

		$user = FrontUser::where('id',$request->get('id'))->value('active');

		if($user == 1)

		{

			$update = FrontUser::where('id',$request->get('id'))->update(['active' => 0]);

		}

		if($user == 0)

		{

			$update = FrontUser::where('id',$request->get('id'))->update(['active' => 1]);

		}

		if($update)

		{

			return response()->json(['status' => 'status_changed']);

		}

	}



	/**

	 * Display the specified resource.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function show($id)

	{

		$data['title'] = 'User';

		$data['edit'] = FrontUser::leftJoin('roles as rl','rl.id','front_user.role_id')

		->select('rl.title','front_user.*')

		->where('role_id',$id)

		->first();

		return view('user.view')->with($data);

	}



	/**

	 * Show the form for editing the specified resource.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function edit($id)

	{

		$data['title'] = 'User';

		$data['edit'] = FrontUser::FindOrFail($id);



		$data['role'] = Role::where('active',1)->get();

		return view('user.edit')->with($data);

	}



	/**

	 * Update the specified resource in storage.

	 *

	 * @param  \Illuminate\Http\Request  $request

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function update(Request $request, $id)

	{

		$param = $request->all();

		if($request->hasFile('document'))

		{

			$document_name = time().'-'.$request->document->getClientOriginalName();

			request()->document->move(public_path('/user_doc/'), $document_name);

			unlink(public_path('/user_doc/'.$request->old_doc));

			$param['document'] = $document_name;

		}else{

			$param['document'] = $request->old_doc;

		}

		unset($param['_token'],$param['_method'],$param['old_doc']);
		
		$update = FrontUser::where('id',$id)->update($param);

		if($update)

		{

			return response()->json(['status' => 'success']);

		}

		else

		{

			return response()->json(['status' => 'error']);

		}

	}



	/**

	 * Remove the specified resource from storage.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function delete(Request $request)

	{

		$delete = FrontUser::where('id',$request->get('id'))->delete();

		if ($delete) 

		{

			return response()->json(['status' => 'success']);

		}

	}



	public function checkEmail(Request $request)

	{

		$param = $request->all();

		if($param['email'] != ""){

			if(isset($param['id'])){

				if($param['id'] > 0){

					$check = FrontUser::where('email',$param['email'])->where('id','!=',$param['id'])->exists();

				}

			}else{

				$check = FrontUser::where('email',$param['email'])->exists();

			}

			if($check){

				echo json_encode(false);

			}else{

				echo json_encode(true);

			}

		}

	}

}

