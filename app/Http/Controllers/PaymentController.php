<?php



namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Review;
use App\Models\FrontUser;

use Str;

use DB;



class PaymentController extends Controller

{

	/**

	 * Display a listing of the resource.

	 *

	 * @return \Illuminate\Http\Response

	 */

	public function __construct(Review $s)

	{

		$this->middleware('auth');

	}



	public function index(Request $request)

	{

		$data['title'] = 'Payment';

		return view('payment.index')->with($data);

	}



	/**

	 * Show the form for creating a new resource.

	 *

	 * @return \Illuminate\Http\Response

	 */

	public function create()

	{

		$data['title'] = "Category";

		return view('category.create')->with($data);

	}



	/**

	 * Store a newly created resource in storage.

	 *

	 * @param  \Illuminate\Http\Request  $request

	 * @return \Illuminate\Http\Response

	 */

	public function store(Request $request)

	{

		$param = $request->all();

		unset($param['_token']);

		$param['slug'] = Str::slug($param['name']);

		$create = Category::create($param);

		if ($create) 

		{

			return redirect()->back();

		} 

		else 

		{

			return redirect()->back();

		}

	}





	public function status(Request $request)

	{

		$role = Category::where('id',$request->get('id'))->value('active');

		if($role == 1)

		{

			$update = Category::where('id',$request->get('id'))->update(['active' => 0]);

			$update = SubCategory::where('cat_id',$request->get('id'))->update(['active' => 0]);

		}

		if($role == 0)

		{

			$update = Category::where('id',$request->get('id'))->update(['active' => 1]);

			$update = SubCategory::where('cat_id',$request->get('id'))->update(['active' => 1]);

		}

		if($update)

		{

			return response()->json(['status' => 'status_changed']);

		}

	}



	/**

	 * Display the specified resource.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function show($id)

	{

		//

	}



	/**

	 * Show the form for editing the specified resource.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function edit(Request $request)

	{

		$data['title'] = "User Role";

		$data['review_data'] = Review::where('id',$request->get('id'))->first();

		return response()->json($data);

	}



	/**

	 * Update the specified resource in storage.

	 *

	 * @param  \Illuminate\Http\Request  $request

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function update(Request $request)

	{

		$param = $request->all();

		unset($param['_token'],$param['review_hidden_id']);

		$update = Review::where('id',$request->review_hidden_id)->update($param);

		return redirect()->back();

	}



	/**

	 * Remove the specified resource from storage.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function delete(Request $request)

	{

		$delete = Review::where('id',$request->get('id'))->delete();
	

		if ($delete)

		{

			return response()->json(['status' => 'success']);

		}

	}

}

