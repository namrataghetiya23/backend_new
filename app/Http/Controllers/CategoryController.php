<?php



namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Category;

use App\Models\SubCategory;

use App\Models\Session;

use App\Models\SessionGroup;

use App\Models\SessionOneToOne;

use App\Models\Course;

use App\Models\CourseChapter;

use App\Models\CourseSession;

use Str;

use DB;



class CategoryController extends Controller

{

	/**

	 * Display a listing of the resource.

	 *

	 * @return \Illuminate\Http\Response

	 */

	public function __construct(Category $s)

	{

		$this->middleware('auth');

	}



	public function index(Request $request)

	{

		$data['title'] = 'Category';

		if($request->category != null){

			$data['data'] = Category::where('type',$request->category)->get();

		}else{

			$data['data'] = Category::get();

		}

		return view('category.index')->with($data);

	}



	/**

	 * Show the form for creating a new resource.

	 *

	 * @return \Illuminate\Http\Response

	 */

	public function create()

	{

		$data['title'] = "Category";

		return view('category.create')->with($data);

	}



	/**

	 * Store a newly created resource in storage.

	 *

	 * @param  \Illuminate\Http\Request  $request

	 * @return \Illuminate\Http\Response

	 */

	public function store(Request $request)

	{

		$param = $request->all();

		unset($param['_token']);

		$param['slug'] = Str::slug($param['name']);

		$create = Category::create($param);

		if ($create) 

		{

			return redirect()->back();

		} 

		else 

		{

			return redirect()->back();

		}

	}





	public function status(Request $request)

	{

		$role = Category::where('id',$request->get('id'))->value('active');

		if($role == 1)

		{

			$update = Category::where('id',$request->get('id'))->update(['active' => 0]);

			$update = SubCategory::where('cat_id',$request->get('id'))->update(['active' => 0]);

		}

		if($role == 0)

		{

			$update = Category::where('id',$request->get('id'))->update(['active' => 1]);

			$update = SubCategory::where('cat_id',$request->get('id'))->update(['active' => 1]);

		}

		if($update)

		{

			return response()->json(['status' => 'status_changed']);

		}

	}



	/**

	 * Display the specified resource.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function show($id)

	{

		//

	}



	/**

	 * Show the form for editing the specified resource.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function edit(Request $request)

	{

		$data['title'] = "User Role";

		$data['category_data'] = Category::where('id',$request->get('id'))->first();

		return response()->json($data);

	}



	/**

	 * Update the specified resource in storage.

	 *

	 * @param  \Illuminate\Http\Request  $request

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function update(Request $request)

	{

		$param = $request->all();

		unset($param['_token'],$param['category_hidden_id']);

		$param['slug'] = Str::slug($param['name']);

		$update = Category::where('id',$request->category_hidden_id)->update($param);

		return redirect()->back();

	}



	/**

	 * Remove the specified resource from storage.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function delete(Request $request)

	{

		$delete_cat = Category::where('id',$request->get('id'))->delete();

		$delete_subcat = SubCategory::where('cat_id',$request->get('id'))->delete();

		$delete_session = Session::where('sub_category',$request->get('id'))->delete();
		
		if(count($delete_session) > 0){
			foreach ($delete_session as $del_sess) {
				$delete_session_grup = SessionGroup::where('session_id',$del_sess->id)->delete();
				$delete_session_oto = SessionOneToOne::where('session_id',$del_sess->id)->delete();
			}	
		}else{
			$delete_session_grup = SessionGroup::where('session_id',$delete_session->id)->delete();
			$delete_session_oto = SessionOneToOne::where('session_id',$delete_session->id)->delete();
		}
		
		$delete_course = Course::where('cat_id',$request->get('id'))->delete();

		if(count($delete_course) > 0){
			foreach ($delete_course as $del_cour) {
				$delete_course_chapter = CourseChapter::where('course_id',$del_cour->id)->delete();
				$delete_course_session = CourseSession::where('course_id',$del_cour->id)->delete();
			}	
		}else{
			$delete_course_chapter = CourseChapter::where('course_id',$delete_course->id)->delete();
			$delete_course_session = CourseSession::where('course_id',$delete_course->id)->delete();
		}

		if ($delete_cat)

		{

			return response()->json(['status' => 'success']);

		}

	}



	public function checkCatName(Request $request)

	{

		$param = $request->all();

		if($param['name'] != ""){

			$param['slug'] = Str::slug($param['name']);

			if(isset($param['id'])){

				if($param['id'] > 0){

					$check = Category::where('slug',$param['slug'])->where('type',$param['type'])->where('id','!=',$param['id'])->exists();

				}

			}else{

				$check = Category::where('slug',$param['slug'])->where('type',$param['type'])->exists();

			}



			if($check){

				echo json_encode(false);

			}else{

				echo json_encode(true);

			}

		}

	}

}

