<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\Models\SubCategory;

use App\Models\Category;

use App\Models\Session;

use App\Models\SessionGroup;

use App\Models\SessionOneToOne;

use App\Models\Course;

use App\Models\CourseChapter;

use App\Models\CourseSession;

use Str;

use DB;



class SubcategoryController extends Controller

{

	/**

	 * Display a listing of the resource.

	 *

	 * @return \Illuminate\Http\Response

	 */

	public function __construct(Category $s)

	{

		$this->middleware('auth');

	}



	public function index(Request $request)

	{

		$data['title'] = 'Sub Category';

		if($request->subCategory != null){

			$data['data'] = SubCategory::leftJoin('category as cat','cat.id','sub_cateogry.cat_id')

			->select('sub_cateogry.*','cat.type','cat.name')

			->where('cat.type',$request->category)

			->get();

		}else{

			$data['data'] = SubCategory::leftJoin('category as cat','cat.id','sub_cateogry.cat_id')

			->select('sub_cateogry.*','cat.name','cat.type')->get();

		}



		$data['category'] = Category::where('active',1)->get();



		return view('subCategory.index')->with($data);

	}



	/**

	 * Show the form for creating a new resource.

	 *

	 * @return \Illuminate\Http\Response

	 */

	public function create()

	{

		$data['title'] = "Category";

		return view('subCategory.create')->with($data);

	}



	/**

	 * Store a newly created resource in storage.

	 *

	 * @param  \Illuminate\Http\Request  $request

	 * @return \Illuminate\Http\Response

	 */

	public function store(Request $request)

	{

		$param = $request->all();

		unset($param['_token']);

		$param['sub_slug'] = Str::slug($param['sub_name']);
		
		$create = SubCategory::create($param);

		if($create)

		{

			return response()->json(['status' => 'success']);

		}

		else

		{

			return response()->json(['status' => 'error']);

		}

	}





	public function status(Request $request)

	{

		$cat_actibe = SubCategory::leftJoin("category as cat",'cat.id','sub_cateogry.cat_id')->where('sub_cateogry.id',$request->get('id'))->value('cat.active');

		

		if($cat_actibe == 1){

			$role = SubCategory::where('id',$request->get('id'))->value('active');

			if($role == 1)

			{

				$update = SubCategory::where('id',$request->get('id'))->update(['active' => 0]);

			}

			if($role == 0)

			{

				$update = SubCategory::where('id',$request->get('id'))->update(['active' => 1]);

			}



			if($update)

			{

				return response()->json(['status' => 'status_changed']);

			}

		}else{

			return response()->json(['status' => 'active_category']);

		}

	}



	/**

	 * Display the specified resource.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function show($id)

	{

		//

	}



	/**

	 * Show the form for editing the specified resource.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function edit(Request $request)

	{

		$data['title'] = "User Role";

		$data['subCategory_data'] = SubCategory::where('id',$request->get('id'))->first();

		return response()->json($data);

	}



	/**

	 * Update the specified resource in storage.

	 *

	 * @param  \Illuminate\Http\Request  $request

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function update(Request $request)

	{

		$param = $request->all();

		unset($param['_token'],$param['subcategory_hidden_id']);

		$param['sub_slug'] = Str::slug($param['sub_name']);

		$update = SubCategory::where('id',$request->subcategory_hidden_id)->update($param);

		

		if($update)

		{

			return response()->json(['status' => 'success']);

		}

		else

		{

			return response()->json(['status' => 'error']);

		}

	}



	/**

	 * Remove the specified resource from storage.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function delete(Request $request)

	{

		$delete_subcat = SubCategory::where('id',$request->get('id'))->delete();

		$delete_session = Session::where('sub_subCategory',$request->get('id'))->delete();
		
		if(count($delete_session) > 0){
			foreach ($delete_session as $del_sess) {
				$delete_session_grup = SessionGroup::where('session_id',$del_sess->id)->delete();
				$delete_session_oto = SessionOneToOne::where('session_id',$del_sess->id)->delete();
			}	
		}else{
			$delete_session_grup = SessionGroup::where('session_id',$delete_session->id)->delete();
			$delete_session_oto = SessionOneToOne::where('session_id',$delete_session->id)->delete();
		}
		
		$delete_course = Course::where('subcat_id',$request->get('id'))->delete();

		if(count($delete_course) > 0){
			foreach ($delete_course as $del_cour) {
				$delete_course_chapter = CourseChapter::where('course_id',$del_cour->id)->delete();
				$delete_course_session = CourseSession::where('course_id',$del_cour->id)->delete();
			}	
		}else{
			$delete_course_chapter = CourseChapter::where('course_id',$delete_course->id)->delete();
			$delete_course_session = CourseSession::where('course_id',$delete_course->id)->delete();
		}

		if ($delete_subcat) 

		{

			return response()->json(['status' => 'success']);

		}

	}



	public function checkSubCatName(Request $request)

	{

		$param = $request->all();

		if($param['sub_name'] != ""){

			$param['sub_slug'] = Str::slug($param['sub_name']);

			if(isset($param['id'])){

				if($param['id'] > 0){

					$check = SubCategory::where('sub_slug',$param['sub_slug'])->where('cat_id',$param['cat_id'])->where('id','!=',$param['id'])->exists();

				}

			}else{

				$check = SubCategory::where('sub_slug',$param['sub_slug'])->where('cat_id',$param['cat_id'])->exists();

			}



			if($check){

				echo json_encode(false);

			}else{

				echo json_encode(true);

			}

		}

	}

}

