<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\Models\Banner;

use Str;

use DB;



class BannerController extends Controller

{

	/**

	 * Display a listing of the resource.

	 *

	 * @return \Illuminate\Http\Response

	 */

	public function __construct(Banner $s)

	{

		$this->middleware('auth');

	}



	public function index()

	{

		$data['title'] = 'Banner';

		$data['data'] = Banner::get();

		return view('banner.index')->with($data);

	}



	/**

	 * Show the form for creating a new resource.

	 *

	 * @return \Illuminate\Http\Response

	 */

	public function create()

	{

		$data['title'] = "User Role";

		return view('role.create')->with($data);

	}



	/**

	 * Store a newly created resource in storage.

	 *

	 * @param  \Illuminate\Http\Request  $request

	 * @return \Illuminate\Http\Response

	 */

	public function store(Request $request)

	{

		$param = $request->all();

		unset($param['user_id']);

		

		if($request->hasFile('image'))

		{

			$doc1 = time().'-'.$request->image->getClientOriginalName();

			request()->image->move(public_path('/banner/'), $doc1);

			$param['image'] = $doc1;

		}

		$create = Banner::create($param);

		Banner::where('id','!=',$create->id)->update(['active'=>0]);

		if ($create) 

		{

			return redirect()->back();

		} 

		else 

		{

			return redirect()->back();

		}

	}





	public function status(Request $request)

	{

		$role = Banner::where('id',$request->get('id'))->value('active');

		if($role == 1)

		{

			$update = Banner::where('id',$request->get('id'))->update(['active' => 0]);

		}

		if($role == 0)

		{

			$update = Banner::where('id',$request->get('id'))->update(['active' => 1]);

		}

		if($update)

		{

			return response()->json(['status' => 'status_changed']);

		}

	}



	/**

	 * Display the specified resource.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function show($id)

	{

		//

	}



	/**

	 * Show the form for editing the specified resource.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function edit(Request $request)

	{

		$data['title'] = "User Role";

		$data['banner_data'] = Banner::where('id',$request->get('id'))->first();

		$data['img'] = asset('/banner/'.$data['banner_data']['image']);
		return response()->json($data);

	}


	/**

	 * Update the specified resource in storage.

	 *

	 * @param  \Illuminate\Http\Request  $request

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function update(Request $request)

	{

		$param['title'] = $request->title;

		if($request->hasFile('image'))

		{

			$image_name = time().'-'.$request->image->getClientOriginalName();

			request()->image->move(public_path('/banner/'), $image_name);

			unlink(public_path('/banner/'.$request->old_image));

			$param['image'] = $image_name;

		}else{

			$param['image'] = $request->old_image;

		}

		$update = Banner::where('id',$request->banner_id)->update($param);

		return redirect()->back();

	}



	/**

	 * Remove the specified resource from storage.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function delete(Request $request)

	{

		

	}



	public function checkRole(Request $request)

	{

		$param = $request->all();

		if($param['title'] != ""){

			$param['title'] = Str::slug($param['title']);

			if($param['type'] == 'Add'){

				$check = Banner::where('slug',$param['title'])->exists();

			}else{

				$check = Banner::where('slug',$param['title'])->where('id','!=',$param['id'])->exists();

			}



			if($check){

				echo json_encode(false);

			}else{

				echo json_encode(true);

			}

		}

	}

}

