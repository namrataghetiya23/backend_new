<?php

namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\Models\Course;

use App\Models\FrontUser;

use App\Models\SubCategory;

use App\Models\Category;

use App\Models\BatchDetail;

use App\Models\SessionDetail;

use Str;

use DB;

class CourseController extends Controller

{


	/**

	 * Display a listing of the resource.

	 *

	 * @return \Illuminate\Http\Response

	 */

	public function __construct(Course $s)
	{

		$this->middleware('auth');
	}



	public function index(Request $request)
	{

		$data['title'] = 'Course';

		$data['data'] = Course::get();

		return view('course.index')->with($data);
	}

	public function create()
	{

		$data['title'] = "Course";

		$data['user'] = FrontUser::where('active',1)->where('role_id',2)->get();

		$data['category'] = Category::where('active',1)->get();

		return view('course.create')->with($data);
	}

	public function store(Request $request)
	{
		// dd($request);

		if($request->hasFile('image'))
		{
			$image_name = time().'-'.$request->image->getClientOriginalName();
			request()->image->move(public_path('/course_img/'), $image_name);
		}

		$add_course = new Course();
		$add_course->user_id = $request->user_id;
		$add_course->title = $request->title;
		$add_course->sub_title = $request->sub_title;
		$add_course->cat_id = $request->cat_id;
		$add_course->subcat_id = $request->subcat_id;
		$add_course->course_image = $image_name;
		$add_course->commission = $request->commission;
		$add_course->cancle_charge_student = $request->cancle_charge_student;
		$add_course->cancle_charge_tutor = $request->cancle_charge_tutor;
		$add_course->save();

		$count_batch = count($request['no_of_attendees']);
		if($count_batch > 0){
			for ($i=0; $i < $count_batch; $i++) { 
				// print_r($request['no_of_attendees'][$i]);die();
				$add_batch_detail = new BatchDetail();
				$add_batch_detail->course_id = $add_course->id;
				$add_batch_detail->attendees = $request['no_of_attendees'][$i];
				$add_batch_detail->course_fees = $request['course_fess'][$i];
				$add_batch_detail->age_group = $request['age_group'][$i];
				$add_batch_detail->save();

				if($request['session_count'][$i] > 0){
					for ($j=0; $j < $request['session_count'][$i]; $j++) {
						$k = $i + 1;

						if($request['from_date_'.$k][$j] == "" || $request['from_date_'.$k][$j] == null){
							$recursive = 0;
						}else{
							$recursive = 1;
						}

						$add_session_detail = new SessionDetail();
						$add_session_detail->course_id = $add_course->id;
						$add_session_detail->batch_id = $add_batch_detail->id;
						$add_session_detail->session_dt_time = $request['session_dt_time_'.$k][$j];
						$add_session_detail->session_type = $request['session_type_'.$k][$j];
						$add_session_detail->recursive = $recursive;
						$add_session_detail->from_dt = $request['from_date_'.$k][$j];
						$add_session_detail->to_dt = $request['to_date_'.$k][$j];
						$add_session_detail->save();
					}
				}
			}
		}
		return response()->json(['status' => 'success']);
	}

	public function status(Request $request)

	{

		$user = Course::where('id',$request->get('id'))->value('active');

		if($user == 1)

		{

			$update = Course::where('id',$request->get('id'))->update(['active' => 0]);

		}

		if($user == 0)

		{

			$update = Course::where('id',$request->get('id'))->update(['active' => 1]);

		}

		if($update)

		{

			return response()->json(['status' => 'status_changed']);

		}

	}

	public function homestatus(Request $request)

	{

		$user = Course::where('id',$request->get('id'))->value('show_on_home');

		if($user == 1)

		{

			$update = Course::where('id',$request->get('id'))->update(['show_on_home' => 0]);

		}

		if($user == 0)

		{

			$update = Course::where('id',$request->get('id'))->update(['show_on_home' => 1]);

		}

		if($update)

		{

			return response()->json(['status' => 'status_changed']);

		}

	}



	/**

	 * Display the specified resource.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function show($id)

	{

		//

	}



	/**

	 * Show the form for editing the specified resource.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function edit($id)

	{

		$data['title'] = "Course";

		$data['user'] = FrontUser::where('active',1)->where('role_id',2)->get();

		$data['category'] = Category::where('active',1)->get();

		$data['course'] = Course::FindOrFail($id);

		$data['subCategory'] = SubCategory::where('cat_id',$data['course']['cat_id'])->where('active',1)->get();



		$data['CourseSession'] = CourseSession::where('course_id',$id)->get();

		$data['CourseChapter'] = CourseChapter::where('course_id',$id)->get();



		return view('course.edit')->with($data);

	}



	/**

	 * Update the specified resource in storage.

	 *

	 * @param  \Illuminate\Http\Request  $request

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function update(Request $request, $id)

	{

		if($request->hasFile('image'))

		{

			$image_name = time().'-'.$request->image->getClientOriginalName();

			request()->image->move(public_path('/course_img/'), $image_name);

			unlink(public_path('/course_img/'.$request->hide_image));

		}else{

			$image_name = $request->hide_image;

		}



		$add_course = Course::Findorfail($id);

		$add_course->user_id = $request->user_id;

		$add_course->title = $request->title;

		$add_course->description = $request->description;

		$add_course->cat_id = $request->cat_id;

		$add_course->subcat_id = $request->subcat_id;

		$add_course->duration = $request->duration;

		$add_course->type = $request->type;

		$add_course->fees = $request->fees;

		$add_course->image = $image_name;

		$add_course->save();



		if($request->type == 1){
			$delete = CourseChapter::where('course_id',$add_course->id)->delete();

			for ($i = 0; $i < count($request['session_title']); $i++) {

				if($request['session_id'][$i] > 0){

					$add_course_session = CourseSession::Findorfail($request['session_id'][$i]);

					$add_course_session->course_id = $add_course->id;

					$add_course_session->session_title = $request['session_title'][$i];

					$add_course_session->session_date_time = date('Y-m-d h:i:s',strtotime($request['session_date_time'][$i]));

					$add_course_session->session_attendees = $request['session_attendees'][$i];

					$add_course_session->save();

				}else{

					$new_course_session = new CourseSession();

					$new_course_session->course_id = $add_course->id;

					$new_course_session->session_title = $request['session_title'][$i];

					$new_course_session->session_date_time = date('Y-m-d h:i:s',strtotime($request['session_date_time'][$i]));

					$new_course_session->session_attendees = $request['session_attendees'][$i];

					$new_course_session->save();

				}

			}

			return response()->json(['status' => 'success']);

		}else if($request->type == 2){

			$delete = CourseSession::where('course_id',$add_course->id)->delete();

			for ($i = 0; $i < count($request['chapter_title']); $i++) {

				if($request['chapter_id'][$i] > 0){

					if($request->hasFile('chapter_document'))

					{

						$chapter_doc_name = time().'-'.$request['chapter_document'][$i]->getClientOriginalName();
						
						$request['chapter_document'][$i]->move(public_path('/course_chapter_img/'), $chapter_doc_name);

						unlink(public_path('/course_chapter_img/'.$request['old_chapter_document'][$i]));

					}



					if($request->hasFile('chapter_video'))

					{

						$chapter_doc_video = time().'-'.$request['chapter_video'][$i]->getClientOriginalName();

						$request['chapter_video'][$i]->move(public_path('/course_chapter_video/'), $chapter_doc_video);

						unlink(public_path('/course_chapter_img/'.$request['old_chapter_video'][$i]));

					}



					$add_course_chapter = CourseChapter()::Findorfail($request['chapter_id'][$i]);;

					$add_course_chapter->course_id = $add_course->id;

					$add_course_chapter->chapter_title = $request['chapter_title'][$i];

					$add_course_chapter->chapter_description = $request['chapter_description'][$i];

					$add_course_chapter->chapter_document = $chapter_doc_name;

					$add_course_chapter->chapter_video = $chapter_doc_video;

					$add_course_chapter->save();

				}else{

					if($request->hasFile('chapter_document'))

					{

						$chapter_doc_name = time().'-'.$request['chapter_document'][$i]->getClientOriginalName();

						$request['chapter_document'][$i]->move(public_path('/course_chapter_img/'), $chapter_doc_name);

					}



					if($request->hasFile('chapter_video'))

					{

						$chapter_doc_video = time().'-'.$request['chapter_video'][$i]->getClientOriginalName();

						$request['chapter_video'][$i]->move(public_path('/course_chapter_video/'), $chapter_doc_video);

					}



					$new_course_chapter = new CourseChapter();

					$new_course_chapter->course_id = $add_course->id;

					$new_course_chapter->chapter_title = $request['chapter_title'][$i];

					$new_course_chapter->chapter_description = $request['chapter_description'][$i];

					$new_course_chapter->chapter_document = $chapter_doc_name;

					$new_course_chapter->chapter_video = $chapter_doc_video;

					$new_course_chapter->save();

				}

			}

			return response()->json(['status' => 'success']);

		}else{

			return response()->json(['status' => 'error']);

		}

	}



	/**

	 * Remove the specified resource from storage.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function delete(Request $request)

	{

		$delete = Course::where('id',$request->get('id'))->delete();
		$delete_chapter = CourseChapter::where('course_id',$delete->id)->delete();
		$delete_session = CourseSession::where('course_id',$delete->id)->delete();

		if ($delete) 

		{

			return response()->json(['status' => 'success']);

		}

	}

	public function changeCommision(Request $request)
	{
		$param = $request->all();
		$count = count($param['idArray']);
		if($param['reqest'] == 1){
			for ($i=0; $i < $count ; $i++) { 
				$update = Course::where('id',$param['idArray'][$i])->update(['commission' => $param['value']]);
			}
		}
		if($param['reqest'] == 2){
			for ($i=0; $i < $count ; $i++) { 
				$update = Course::where('id',$param['idArray'][$i])->update(['cancle_charge_student' => $param['value']]);
			}
		}
		if($param['reqest'] == 3){
			for ($i=0; $i < $count ; $i++) { 
				$update = Course::where('id',$param['idArray'][$i])->update(['cancle_charge_tutor' => $param['value']]);
			}
		}

		if($update){
			return response()->json(['status' => 'success']);
		}else{
			return response()->json(['status' => 'error']);
		}
	}

	public function getSubcate(Request $request)

	{

		$param = $request->all();

		

		$sub_cat = SubCategory::where('cat_id',$param['cat_id'])->where('active',1)->get();

		$html = "<option value=''>--Select Sub Category--</option>";

		if(!empty($sub_cat)){

			foreach ($sub_cat as $sub_cat_data) {

				$html .= "<option value='".$sub_cat_data->id."'>".$sub_cat_data->sub_name."</option>";

			}

		}

		echo $html;

	}

}

