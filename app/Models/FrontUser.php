<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FrontUser extends Model
{
	use SoftDeletes;
	protected $table = 'front_user';
	protected $primaryKey = 'id';
	protected $guarded = ['id'];
}
