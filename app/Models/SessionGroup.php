<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SessionGroup extends Model
{
	use SoftDeletes;
    protected $table = 'session_group';
	protected $primaryKey = 'id';
	protected $guarded = ['id'];
}