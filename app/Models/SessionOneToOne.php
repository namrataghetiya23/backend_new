<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SessionOneToOne extends Model
{
	use SoftDeletes;
    protected $table = 'session_oneTo_one';
	protected $primaryKey = 'id';
	protected $guarded = ['id'];
}