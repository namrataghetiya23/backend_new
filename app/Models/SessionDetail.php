<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SessionDetail extends Model
{
	use SoftDeletes;
    protected $table = 'session_details';
	protected $primaryKey = 'id';
	protected $guarded = ['id'];
}