<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BatchDetail extends Model
{
	use SoftDeletes;
    protected $table = 'batch_detail';
	protected $primaryKey = 'id';
	protected $guarded = ['id'];
}