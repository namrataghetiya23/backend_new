<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseChapter extends Model
{
	use SoftDeletes;
    protected $table = 'course_chapter';
	protected $primaryKey = 'id';
	protected $guarded = ['id'];
}