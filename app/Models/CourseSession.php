<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseSession extends Model
{
	use SoftDeletes;
    protected $table = 'course_session';
	protected $primaryKey = 'id';
	protected $guarded = ['id'];
}