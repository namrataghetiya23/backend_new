<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
	return view('auth.login');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('roles','RoleController');
Route::post('roles/update','RoleController@update')->name('update.roles');
Route::post('roles/edits','RoleController@edit')->name('roles.edits');
Route::any('roles/status','RoleController@status')->name('role.status');

Route::resource('user','UserController');
Route::post('user/delete','UserController@delete')->name('delete');
Route::post('user/indexs', 'UserController@index')->name('user.indexs');
Route::post('check/email','UserController@checkEmail')->name('check.email');
Route::any('user/status','UserController@status')->name('user.status');

Route::resource('admin','AdminController');
Route::post('admin/delete','AdminController@delete')->name('delete');
Route::post('check/admin/email','AdminController@checkEmail')->name('check.adminEmail');
Route::any('admin/status','AdminController@status')->name('admin.status');

Route::resource('banner','BannerController');
Route::post('banner/update','BannerController@update')->name('update.banner');
Route::post('banner/edits','BannerController@edit')->name('edits');
Route::any('banner/status','BannerController@status')->name('banner.status');

Route::resource('category','CategoryController');
Route::post('category/update','CategoryController@update')->name('update.category');
Route::post('category/edits','CategoryController@edit')->name('category.edits');
Route::any('category/status','CategoryController@status')->name('category.status');
Route::post('category/indexs', 'CategoryController@index')->name('category.indexs');
Route::post('category/delete','CategoryController@delete')->name('delete.category');
Route::post('check/catName','CategoryController@checkCatName')->name('check.catName');

Route::resource('subCategory','SubcategoryController');
Route::post('subCategory/update','SubcategoryController@update')->name('update.subCategory');
Route::post('subCategory/edits','SubcategoryController@edit')->name('subCategory.edits');
Route::any('subCategory/status','SubcategoryController@status')->name('subCategory.status');
Route::post('subCategory/indexs', 'SubcategoryController@index')->name('subCategory.indexs');
Route::post('subCategory/delete','SubcategoryController@delete')->name('delete.subCategory');
Route::post('check/subCatName','SubcategoryController@checkSubCatName')->name('check.subCatName');

Route::resource('course','CourseController');
Route::post('course/delete','CourseController@delete')->name('course.delete');
Route::any('course/status','CourseController@status')->name('course.status');
Route::any('course/homestatus','CourseController@homestatus')->name('course.homestatus');
Route::post('get/subCate', 'CourseController@getSubcate')->name('get.subCate');
Route::post('course/changeCommision', 'CourseController@changeCommision')->name('course.changeCommision');

Route::resource('staticPage','StaticPageController');
Route::post('staticPage/delete','StaticPageController@delete')->name('staticPage.delete');
Route::post('staticPage/indexs', 'StaticPageController@index')->name('staticPage.indexs');
Route::any('staticPage/status','StaticPageController@status')->name('staticPage.status');
Route::post('check/title','StaticPageController@checkTitle')->name('check.title');

Route::resource('testimonials','TestimonialsController');
Route::post('testimonials/delete','TestimonialsController@delete')->name('testimonials.delete');
Route::post('testimonials/indexs', 'TestimonialsController@index')->name('testimonials.indexs');
Route::any('testimonials/status','TestimonialsController@status')->name('testimonials.status');

Route::resource('profile','ProfileController');
Route::get('changePassword','ProfileController@changePassword')->name('changePassword');
Route::post('check/password','ProfileController@checkPassword')->name('check.password');

Route::resource('session','SessionController');
Route::post('session/delete','SessionController@delete')->name('session.delete');
Route::any('session/status','SessionController@status')->name('session.status');

Route::resource('review','ReviewController');
Route::post('review/update','ReviewController@update')->name('update.review');
Route::post('review/edits','ReviewController@edit')->name('review.edits');
Route::any('review/status','ReviewController@status')->name('review.status');
Route::post('review/indexs', 'ReviewController@index')->name('review.indexs');
Route::post('review/delete','ReviewController@delete')->name('delete.review');

Route::resource('payment','PaymentController');
Route::resource('redemption','RedemptionController');